import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer
import AVKit
import GoogleMobileAds
import ARSLineProgress

class SpeedViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, MPMediaPickerControllerDelegate,  UICollectionViewDelegate , UICollectionViewDataSource ,GADBannerViewDelegate {
    
    var imageThumbnail              = UIImage()
    var videoPathURL                = NSURL()
    let identifier                  = "CellIdentifier"
    var latestFilePath              = NSString()
    var playerViewController        = AVPlayerViewController()
    var blackBackgroundNewHeight    = UIScreen.mainScreen().bounds.size.width - 16
    var playVid                     = NSURL()
    var player                      = AVPlayer()
    var playerLayer                 = AVPlayerLayer()
    let date                        = NSDate()
    let calendar                    = NSCalendar.currentCalendar()
    let finalURL                    = NSString()
    var invisibleButton             = UIButton()
    var newHeight                   : CGFloat?
    let screenSize                  = UIScreen.mainScreen().bounds.size.height - 207
    var bannerView                  : GADBannerView!
    var audioURL                    : NSURL?
    var audioEngine                 : AVAudioEngine!
    var audioFile                   : AVAudioFile!
    
    @IBOutlet weak var imageView            : UIImageView!
    @IBOutlet weak var collectionView       : UICollectionView!
    @IBOutlet weak var backgroundView       : UIView!
    @IBOutlet weak var blackBackgroundView  : UIView!
    @IBOutlet weak var playimageView        : UIImageView!
    @IBOutlet weak var bBViewHeight         : NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight      : NSLayoutConstraint!
    @IBOutlet weak var saveVideo            : UIBarButtonItem!
    @IBOutlet weak var spinnerView          : UIView!
    @IBOutlet weak var imageViewWidth       : NSLayoutConstraint!
    @IBOutlet weak var bBViewWidth          : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playVid                     = videoPathURL
        imageView.image             = imageThumbnail
        ThemeColor()
        let screenSize                                      = UIScreen.mainScreen().bounds
        let collectionViewFlowLayout                        = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewFlowLayout.itemSize                   = CGSizeMake(screenSize.width / 5.0, screenSize.height / 5.0)
        collectionViewFlowLayout.minimumInteritemSpacing    = 0.0
        collectionViewFlowLayout.minimumLineSpacing         = 0.0
        collectionViewFlowLayout.sectionInset               = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        
        let tapGestureRecognizer                = UITapGestureRecognizer(target:self, action:#selector(SpeedViewController.imageTapped))
        playimageView.userInteractionEnabled    = true
        playimageView.addGestureRecognizer(tapGestureRecognizer)
        
        imageView.addSubview(invisibleButton)
        invisibleButton.addTarget(self, action: #selector(SpeedViewController.invisibleButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        audioFromVideo()
        getNewVideoOrientation()
    }
    
    override func viewWillAppear(animated: Bool) {
        ThemeColor()
    }
    
    override func viewDidDisappear(animated: Bool) {
        playerViewController.view.removeFromSuperview()
        player.pause()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        invisibleButton.frame = imageView.bounds
    }
    
    func getNewVideoOrientation() -> UIImageOrientation {
        let asset       = AVAsset(URL: self.videoPathURL)
        let videoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let size        = videoTrack.naturalSize
        let txf         = videoTrack.preferredTransform
        
        if (size.width == txf.tx && size.height == txf.ty) {
            print("***   Video orientation is left")
            return UIImageOrientation.Left
        }else if (txf.tx == 0 && txf.ty == 0) {
            print("***   Video orientation is right")
            return UIImageOrientation.Right
        }else if (txf.tx == 0 && txf.ty == size.width) {
            print("***   Video orientation is dowm")
            return UIImageOrientation.Down
        }else {
            print("***   Video orientation is up")
            return UIImageOrientation.Up
        }
    }
    
    func audioFromVideo() {
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let audioAsset          = AVURLAsset(URL: self.videoPathURL)
        let startTime           = kCMTimeZero
        let endTime             = audioAsset.duration
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        let audioPath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)audio.M4A")
        audioURL                = NSURL.fileURLWithPath(audioPath as String)
        let timeRange           = CMTimeRangeMake(startTime, endTime)
        
        let exporter : AVAssetExportSession  = AVAssetExportSession(asset: audioAsset, presetName: AVAssetExportPresetAppleM4A)!
        exporter.outputURL                   = audioURL
        exporter.timeRange                   = timeRange
        exporter.outputFileType              = AVFileTypeAppleM4A
        exporter.shouldOptimizeForNetworkUse = true
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinishAudio(exporter)
            } )
        }
    }
    
    func exportDidFinishAudio(session : AVAssetExportSession) -> NSURL {
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed - Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        if(session.status == .Completed){
            audioURL       = session.outputURL!
            print("Audio URL : ",audioURL)
        }else{
           print("This Video Has No Audio")
        }
        return audioURL!
    }
    
    func varySpeedOfAudio(){
        audioEngine = AVAudioEngine()
        do{
           try audioFile   = AVAudioFile(forReading: audioURL!)
        }catch{
            print("Audio File Unable To Read")
        }
        
        
    }
    
    // MARK: SLOW VIDEO BY 200 PERCENTAGE
    func slowVideoTwice() {
        playerLayer.hidden      = true
        spinnerView.hidden      = false
        playimageView.hidden    = true
        
        let videoAsset              = AVURLAsset(URL: videoPathURL)
        let mixComposition          = AVMutableComposition()
        let compositionVideoTrack   = mixComposition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        do {
            try compositionVideoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), ofTrack: (videoAsset.tracksWithMediaType(AVMediaTypeVideo).first)!, atTime: kCMTimeZero)
        } catch {
            return
        }
        
        let videoDuration = videoAsset.duration
        let finalTimeScale : Int64 = videoDuration.value * 2
        
        compositionVideoTrack.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, videoDuration), toDuration: CMTimeMake(finalTimeScale, videoDuration.timescale))
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let finalURL    = documentsURL.URLByAppendingPathComponent("\(hour)\(minutes)\(seconds)slowed.mov")
        latestFilePath  = finalURL.absoluteString
        self.playVid    = finalURL
        let manager     = NSFileManager()
        
        do {
            try manager.removeItemAtPath(finalURL.path!)
        } catch {
        }
        let exporter        = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = finalURL
        exporter!.outputFileType = AVFileTypeQuickTimeMovie
        exporter!.exportAsynchronouslyWithCompletionHandler({
            switch exporter!.status{
            case  AVAssetExportSessionStatus.Failed:
                dispatch_async(dispatch_get_main_queue(), {
                    let alertController: UIAlertController = UIAlertController(title: "Error Exporting Video", message: "\(exporter!.error) \(exporter!.error!.description). Please try again", preferredStyle: .Alert)
                    
                    let okayAction: UIAlertAction = UIAlertAction(title: "Okay", style: .Cancel) { action -> Void in
                    }
                    alertController.addAction(okayAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
                
            case AVAssetExportSessionStatus.Cancelled:
                dispatch_async(dispatch_get_main_queue(), {
                    let alertController: UIAlertController = UIAlertController(title: "Error Exporting Video", message: "\(exporter!.error) \(exporter!.error!.description). Please try again", preferredStyle: .Alert)
                    let okayAction: UIAlertAction = UIAlertAction(title: "Okay", style: .Cancel) { action -> Void in
                    }
                    alertController.addAction(okayAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            default:
                dispatch_async(dispatch_get_main_queue(), {
                    self.checkOrientation(finalURL)
                })
            }
        })
    }
    
    // MARK: SLOW VIDEO BY 400 PERCENTAGE
    func slowVideoFourTimes() {
        spinnerView.hidden     = false
        playimageView.hidden   = true
        playerLayer.hidden     = true
        
        let videoAsset              = AVURLAsset(URL: videoPathURL)
        let mixComposition          = AVMutableComposition()
        let compositionVideoTrack   = mixComposition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        do {
            try compositionVideoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), ofTrack: (videoAsset.tracksWithMediaType(AVMediaTypeVideo).first)!, atTime: kCMTimeZero)
        } catch {
            return
        }
        let videoDuration = videoAsset.duration
        let finalTimeScale : Int64 = videoDuration.value * 4
        
        compositionVideoTrack.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, videoDuration), toDuration: CMTimeMake(finalTimeScale, videoDuration.timescale))
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let finalURL    = documentsURL.URLByAppendingPathComponent("\(hour)\(minutes)\(seconds)slowed.mov")
        latestFilePath  = finalURL.absoluteString
        self.playVid    = finalURL
        let manager     = NSFileManager()
        do {
            try manager.removeItemAtPath(finalURL.path!)
        } catch {
        }
        let exporter        = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = finalURL
        exporter!.outputFileType = AVFileTypeQuickTimeMovie
        exporter!.exportAsynchronouslyWithCompletionHandler({
            switch exporter!.status{
            case  AVAssetExportSessionStatus.Failed:
                dispatch_async(dispatch_get_main_queue(), {
                    let alertController: UIAlertController = UIAlertController(title: "Error Exporting Video", message: "\(exporter!.error) \(exporter!.error!.description). Please try again", preferredStyle: .Alert)
                    
                    let okayAction: UIAlertAction = UIAlertAction(title: "Okay", style: .Cancel) { action -> Void in
                    }
                    alertController.addAction(okayAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            case AVAssetExportSessionStatus.Cancelled:
                dispatch_async(dispatch_get_main_queue(), {
                    let alertController: UIAlertController = UIAlertController(title: "Error Exporting Video", message: "\(exporter!.error) \(exporter!.error!.description). Please try again", preferredStyle: .Alert)
                    
                    let okayAction: UIAlertAction = UIAlertAction(title: "Okay", style: .Cancel) { action -> Void in
                    }
                    alertController.addAction(okayAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            default:
                dispatch_async(dispatch_get_main_queue(), {
                    self.checkOrientation(finalURL)
                })
            }
        })
    }
    
    // MARK: FAST VIDEO BY 50 PERCENTAGE
    func fastVideoHalf() {
        spinnerView.hidden      = false
        playimageView.hidden    = true
        playerLayer.hidden      = true
        
        let videoAsset = AVURLAsset(URL: videoPathURL)
        let mixComposition = AVMutableComposition()
        let compositionVideoTrack = mixComposition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
        do {
            try compositionVideoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), ofTrack: (videoAsset.tracksWithMediaType(AVMediaTypeVideo).first)!, atTime: kCMTimeZero)
        } catch {
            return
        }
        
        let videoDuration  = videoAsset.duration
        let finalTimeScale = videoDuration.value * 1/2
        
        compositionVideoTrack.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, videoDuration), toDuration: CMTimeMake(finalTimeScale, videoDuration.timescale))
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let finalURL = documentsURL.URLByAppendingPathComponent("\(hour)\(minutes)\(seconds)fast.mov")
        self.playVid = finalURL
        let manager = NSFileManager()
        do {
            try manager.removeItemAtPath(finalURL.path!)
        } catch {
        }
        
        let exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = finalURL
        exporter!.outputFileType = AVFileTypeQuickTimeMovie
        exporter!.exportAsynchronouslyWithCompletionHandler({
            switch exporter!.status{
            case  AVAssetExportSessionStatus.Failed:
                dispatch_async(dispatch_get_main_queue(), {
                    
                    let alertController: UIAlertController = UIAlertController(title: "Error Exporting Video", message: "\(exporter!.error) \(exporter!.error!.description). Please try again", preferredStyle: .Alert)
                    
                    let okayAction: UIAlertAction = UIAlertAction(title: "Okay", style: .Cancel) { action -> Void in
                    }
                    alertController.addAction(okayAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
                
            case AVAssetExportSessionStatus.Cancelled:
                dispatch_async(dispatch_get_main_queue(), {
                    let alertController: UIAlertController = UIAlertController(title: "Error Exporting Video", message: "\(exporter!.error) \(exporter!.error!.description). Please try again", preferredStyle: .Alert)
                    
                    let okayAction: UIAlertAction = UIAlertAction(title: "Okay", style: .Cancel) { action -> Void in
                    }
                    alertController.addAction(okayAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            default:
                dispatch_async(dispatch_get_main_queue(), {
                    self.checkOrientation(finalURL)
                })
            }
        })
    }
    
    // MARK: FAST VIDEO BY 75 PERCENTAGE
    func fastVideoQuaterly() {
        spinnerView.hidden      = false
        playimageView.hidden    = true
        playerLayer.hidden      = true
        
        let videoAsset              = AVURLAsset(URL: videoPathURL)
        let mixComposition          = AVMutableComposition()
        let compositionVideoTrack   = mixComposition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
        do {
            try compositionVideoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), ofTrack: (videoAsset.tracksWithMediaType(AVMediaTypeVideo).first)!, atTime: kCMTimeZero)
        } catch {
            return
        }
        
        let videoDuration  = videoAsset.duration
        let finalTimeScale = videoDuration.value * 1/4
        
        compositionVideoTrack.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, videoDuration), toDuration: CMTimeMake(finalTimeScale, videoDuration.timescale))
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let finalURL = documentsURL.URLByAppendingPathComponent("\(hour)\(minutes)\(seconds)fast.mov")
        self.playVid = finalURL
        let manager = NSFileManager()
        do {
            try manager.removeItemAtPath(finalURL.path!)
        } catch {
        }
        
        let exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = finalURL
        exporter!.outputFileType = AVFileTypeQuickTimeMovie
        exporter!.exportAsynchronouslyWithCompletionHandler({
            switch exporter!.status{
            case  AVAssetExportSessionStatus.Failed:
                dispatch_async(dispatch_get_main_queue(), {
                    let alertController: UIAlertController = UIAlertController(title: "Error Exporting Video", message: "\(exporter!.error) \(exporter!.error!.description). Please try again", preferredStyle: .Alert)
                    
                    let okayAction: UIAlertAction = UIAlertAction(title: "Okay", style: .Cancel) { action -> Void in
                    }
                    alertController.addAction(okayAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
                
            case AVAssetExportSessionStatus.Cancelled:
                dispatch_async(dispatch_get_main_queue(), {
                    let alertController: UIAlertController = UIAlertController(title: "Error Exporting Video", message: "\(exporter!.error) \(exporter!.error!.description). Please try again", preferredStyle: .Alert)
                    
                    let okayAction: UIAlertAction = UIAlertAction(title: "Okay", style: .Cancel) { action -> Void in
                    }
                    alertController.addAction(okayAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            default:
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.checkOrientation(finalURL)
                })
            }
        })
    }
    
    func checkOrientation(videoURL : NSURL){
        if getNewVideoOrientation() == getOldVideoOrientation(videoURL) {
            playVid = videoURL
            self.spinnerView.hidden   = true
            self.playimageView.hidden = false
            self.playerLayer.hidden   = true
            self.imageView.image      = self.imageThumbnail
            self.imageView.reloadInputViews()
        }else {
            rotateVideoLeft(videoURL)
        }
    }
    
    func rotateVideoLeft(url : NSURL){
        let asset           = AVURLAsset(URL: url)
        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        
        let videoSizeMain           = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        let cropSquare              = CGRectMake(0, 0, videoSizeMain.width  , videoSizeMain.height)
        videoComposition.renderSize = CGSizeMake(cropSquare.size.height , cropSquare.size.width)
        
        let instruction       = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration)
        let layerInstruction  = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        var t1 = CGAffineTransformIdentity
        var t2 = CGAffineTransformIdentity
        
        t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height , 0)
        t2 = CGAffineTransformRotate(t1, CGFloat(M_PI_2))
        
        let finalTranform = t2
        
        layerInstruction.setTransform(finalTranform, atTime: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // save rotated video
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)rotatedleft.mov")
        self.latestFilePath    = filePath
        let finalFileURL1 : NSURL!
        finalFileURL1          = NSURL.fileURLWithPath(latestFilePath as String)
        
        let exporter                = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.videoComposition   = videoComposition
        exporter.outputURL          = finalFileURL1
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        if NSFileManager.defaultManager().fileExistsAtPath(exporter.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinishChecking(exporter)
                self.spinnerView.hidden   = true
                self.playimageView.hidden = false
                self.playerLayer.hidden   = true
                self.imageView.image      = self.imageThumbnail
                self.imageView.reloadInputViews()
            } )
        }
    }
    
    func exportDidFinishChecking(session : AVAssetExportSession) -> Void {
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed Exporting the video *")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed")
            print("Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        
        if(session.status == .Completed){
            playVid = session.outputURL!
            print("*** url of the reversed video",playVid)
        }
    }

    func getOldVideoOrientation(url : NSURL) -> UIImageOrientation {
        let asset       = AVAsset(URL: url)
        let videoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let size        = videoTrack.naturalSize
        let txf         = videoTrack.preferredTransform
        
        if (size.width == txf.tx && size.height == txf.ty) {
            print("***   Video orientation is left")
            return UIImageOrientation.Left
        }else if (txf.tx == 0 && txf.ty == 0) {
            print("***   Video orientation is right")
            return UIImageOrientation.Right
        }else if (txf.tx == 0 && txf.ty == size.width) {
            print("***   Video orientation is dowm")
            return UIImageOrientation.Down
        }else {
            print("***   Video orientation is up")
            return UIImageOrientation.Up
        }
    }

    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath)
        cell.layer.cornerRadius = 20.0
        cell.layer.masksToBounds = true
        let imageArray = ["video025","video05" ,"originalRotationIcon","video2xIcon","video04"]
        (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named: "\(imageArray[indexPath.item])")
        let labelArray = [".25X",".5X","ORIGINAL","2X","4X"]
        (cell.contentView.viewWithTag(20) as! UILabel).text = labelArray[indexPath.item]
        (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        cell.backgroundColor = UIColor.whiteColor()
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell : UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        if indexPath.item == 0 {
            player.pause()
            slowVideoFourTimes()
            ARSLineProgress.show()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"video025_selected")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
        }else if indexPath.item == 1 {
            player.pause()
            slowVideoTwice()
            ARSLineProgress.show()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"video05_selected")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            
        }else if indexPath.item == 2 {
            player.pause()
            playVid = videoPathURL
            ARSLineProgress.show()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"originalVideoIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            
        }else if indexPath.item == 3 {
            player.pause()
            fastVideoHalf()
            ARSLineProgress.show()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"video2x_selected")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            
        }else if indexPath.item == 4 {
            player.pause()
            fastVideoQuaterly()
            ARSLineProgress.show()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"video04_selected")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell : UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        if indexPath.item == 0{
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"video025")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
            
        } else if indexPath.item == 1 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"video05")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
            
        }else if indexPath.item == 2 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"originalRotationIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
            
        }else if indexPath.item == 3 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"video2xIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
            
        }else if indexPath.item == 4 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"video04")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        }
    }
    
    @IBAction func saveVideo(sender: AnyObject) {
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setURL(playVid, forKey: "VideoNSURL")
        navigationController?.popViewControllerAnimated(true)
    }
    
    func ThemeColor() {
        navigationController!.navigationBar.barTintColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        backgroundView.backgroundColor = UIColor(red: 223/255 , green: 223/255 , blue: 223/255 , alpha: 1.0)
        collectionView.backgroundColor = UIColor(red: 255/255 , green: 255/255 , blue: 255/255 , alpha: 1.0)
        
        let oldWidth                       = imageThumbnail.size.width
        let scaleFactor                    = (UIScreen.mainScreen().bounds.size.width - 30)/oldWidth
        newHeight                          = imageThumbnail.size.height*scaleFactor
        if newHeight < screenSize {
            imageViewHeight.constant       = newHeight!
            bBViewHeight.constant          = newHeight!
            imageViewWidth.constant        = blackBackgroundNewHeight
            bBViewWidth.constant           = blackBackgroundNewHeight
        } else {
            imageViewHeight.constant       = screenSize
            bBViewHeight.constant          = screenSize
            let sfVideo                    = imageThumbnail.size.height/imageThumbnail.size.width
            imageViewWidth.constant        = screenSize/sfVideo
            bBViewWidth.constant           = screenSize/sfVideo
        }
        
        ARSLineProgress.showOnView(spinnerView)
        spinnerView.layer.cornerRadius  = 10.0
        spinnerView.hidden              = true
        
        let origin = CGPointMake(0 , UIScreen.mainScreen().bounds.size.height - (CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height + 120))
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait,origin: origin)
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3589571889245561/6463841756"
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kDFPSimulatorID,"a976aeed5251d82899db95f6bc4ab1a7","55a52af80cba60b60e3045dfa9bfcd9d"]
        bannerView.loadRequest(request)
        view.addSubview(bannerView)
    }
    
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("add appeared")
        
        bannerView.hidden = false
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        bannerView.hidden = true
        print("add failed to appear", error.localizedDescription)
    }
    
    func invisibleButtonTapped(sender: UIButton!) {
        let playerIsPlaying:Bool = player.rate > 0
        if (playerIsPlaying) {
            player.pause();
        } else {
            player.play();
        }
    }
    
    func imageTapped(){
        player              = AVPlayer(URL: playVid)
        playerLayer         = AVPlayerLayer(player: player)
        playerLayer.frame   = self.imageView.bounds
        self.imageView.layer.addSublayer(playerLayer)
        player.play()
        playimageView.hidden = true
        
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(AudioViewController.playerItemDidReachEnd(_:)),name: AVPlayerItemDidPlayToEndTimeNotification,object: player.currentItem)
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            playerItem.seekToTime(kCMTimeZero)
        }
    }
}