import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer
import GoogleMobileAds

class FrontViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate , GADBannerViewDelegate {
    
    let videoPicker         = UIImagePickerController()
    var imageViewPreview    = UIImage()
    var path                = NSURL()
    let cameraController    = UIImagePickerController()
    let fileManager         = NSFileManager.defaultManager()
    var docDir              = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
    var appSuppDir          = NSFileManager.defaultManager().URLsForDirectory(.ApplicationDirectory, inDomains: .UserDomainMask) as [NSURL]
    var bannerView          : GADBannerView!
    
    @IBOutlet weak var gallaryPickerButton    : UIButton!
    @IBOutlet weak var newVidPickerButton     : UIButton!
    @IBOutlet weak var newVideoButtonView     : UIView!
    @IBOutlet weak var gallaryVideoButtonView : UIView!
    @IBOutlet weak var recentVideoButtonView  : UIView!
    @IBOutlet weak var recentVideoButton      : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let size1: CGFloat = min(newVideoButtonView.frame.height, newVideoButtonView.frame.width)
        newVideoButtonView.layer.cornerRadius   = size1/2
        newVideoButtonView.layer.borderWidth    = 2
        newVideoButtonView.layer.borderColor    = UIColor.whiteColor().CGColor
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(FrontViewController.newVideoButtonClicked(_:)))
        self.newVideoButtonView.addGestureRecognizer(gesture1)
        
        let size2: CGFloat = min(gallaryVideoButtonView.frame.height, gallaryVideoButtonView.frame.width)
        gallaryVideoButtonView.layer.cornerRadius   = size2/2
        gallaryVideoButtonView.layer.borderWidth    = 2
        gallaryVideoButtonView.layer.borderColor    = UIColor.whiteColor().CGColor
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(FrontViewController.galleryButtonClicked(_:)))
        self.gallaryVideoButtonView.addGestureRecognizer(gesture2)
        
        let size3: CGFloat = min(recentVideoButtonView.frame.height, recentVideoButtonView.frame.width)
        recentVideoButtonView.layer.cornerRadius    = size3/2
        recentVideoButtonView.layer.borderWidth     = 2
        recentVideoButtonView.layer.borderColor     = UIColor.whiteColor().CGColor
        let gesture3 = UITapGestureRecognizer(target: self, action: #selector(FrontViewController.recentVideoButtonClicked(_:)))
        self.recentVideoButtonView.addGestureRecognizer(gesture3)
        
        let origin = CGPointMake(0 , UIScreen.mainScreen().bounds.size.height - (CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height ))
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait,origin: origin)
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3589571889245561/6463841756"
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kDFPSimulatorID,"a976aeed5251d82899db95f6bc4ab1a7","55a52af80cba60b60e3045dfa9bfcd9d"]
        bannerView.loadRequest(request)
        view.addSubview(bannerView)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        let nsurl = NSUserDefaults.standardUserDefaults()
        if nsurl.URLForKey("VideoNSURL") != nil {
            nsurl.removeObjectForKey("VideoNSURL")
        }
        
        gallaryVideoButtonView.layer.backgroundColor    = UIColor.clearColor().CGColor
        self.gallaryVideoButtonView.layer.shadowRadius  = 0
        gallaryVideoButtonView.center.x -= view.bounds.width
        gallaryPickerButton.center.x    -= view.bounds.width
        
        newVideoButtonView.layer.backgroundColor    = UIColor.clearColor().CGColor
        self.newVideoButtonView.layer.shadowRadius  = 0
        newVideoButtonView.center.y -= view.bounds.height
        newVidPickerButton.center.y -= view.bounds.height
        
        recentVideoButtonView.layer.backgroundColor     = UIColor.clearColor().CGColor
        self.recentVideoButtonView.layer.shadowRadius   = 0
        recentVideoButtonView.center.x  += view.bounds.width
        recentVideoButton.center.x      += view.bounds.width
        
        self.navigationController?.navigationBarHidden = true
        
        UIView.animateWithDuration(1, animations:{
            self.gallaryVideoButtonView.center.x    += self.view.bounds.width
            self.gallaryPickerButton.center.x       += self.view.bounds.width
            
            self.newVideoButtonView.center.y        += self.view.bounds.height
            self.newVidPickerButton.center.y        += self.view.bounds.height
            
            self.recentVideoButtonView.center.x     -= self.view.bounds.width
            self.recentVideoButton.center.x         -= self.view.bounds.width
        })
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        let nsurl = NSUserDefaults.standardUserDefaults()
        if nsurl.URLForKey("VideoNSURL") != nil {
            nsurl.removeObjectForKey("VideoNSURL")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("add appeared")
        
        bannerView.hidden = false
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        bannerView.hidden = true
        print("add failed to appear", error.localizedDescription)
    }

    
    @IBAction func galleryButtonClicked(sender: AnyObject) {
        videoPicker.delegate        = self
        videoPicker.sourceType      = UIImagePickerControllerSourceType.SavedPhotosAlbum
        videoPicker.mediaTypes      = [kUTTypeMovie as String]
        videoPicker.allowsEditing   = false
        
        UIView.animateWithDuration(0, animations: {
            self.gallaryVideoButtonView.layer.shadowOpacity = 100
            self.gallaryVideoButtonView.layer.shadowOffset  = CGSize(width: 0 , height: 0)
            self.gallaryVideoButtonView.layer.shadowRadius  = 10
            self.gallaryVideoButtonView.layer.shadowColor   = UIColor.whiteColor().CGColor
        })
        
        self.presentViewController(videoPicker, animated: true , completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]){
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        picker.dismissViewControllerAnimated(true, completion: nil)
        print("\((info[UIImagePickerControllerMediaURL] as! NSURL))")
        if mediaType == kUTTypeMovie {
            path                                            = (info[UIImagePickerControllerMediaURL] as! NSURL)
            let asset : AVAsset                             = AVAsset(URL: path)
            let assetImgGenerate : AVAssetImageGenerator    = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            let time        : CMTime                        = CMTimeMake(10, 10)
            var img         : CGImageRef!
            do {
                img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
            } catch  {
            }
            
            let frameImg    : UIImage   = UIImage(CGImage: img)
            frameImg
            imageViewPreview            = frameImg
            self.dismissViewControllerAnimated(true, completion: nil)
            
            performSegueWithIdentifier("display", sender: self)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func startCameraFromViewController(viewController: UIViewController, withDelegate delegate: protocol<UIImagePickerControllerDelegate, UINavigationControllerDelegate>) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.Camera) == false {
            return false
        }
        cameraController.sourceType     = .Camera
        cameraController.mediaTypes     = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing  = false
        cameraController.delegate       = delegate
        
        presentViewController(cameraController, animated: true, completion: nil)
        return true
    }
    
    @IBAction func newVideoButtonClicked(sender: AnyObject) {
        startCameraFromViewController(self, withDelegate: self)

        UIView.animateWithDuration(0, animations: {
            self.newVideoButtonView.layer.shadowOpacity = 100
            self.newVideoButtonView.layer.shadowOffset  = CGSize(width: 0 , height: 0)
            self.newVideoButtonView.layer.shadowRadius  = 10
            self.newVideoButtonView.layer.shadowColor   = UIColor.whiteColor().CGColor
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier        == "display") {
            let controller          = segue.destinationViewController as! DisplayViewController
            controller.image        = imageViewPreview
            controller.videoURL     = path
        } else if (segue.identifier == "DisplayRecent") {
            let nav                 = segue.destinationViewController as! UINavigationController
            let controller          = nav.topViewController as! MyCollectionViewController
            controller.docDir       = docDir
        }
    }
    
    @IBAction func recentVideoButtonClicked(sender: AnyObject) {
        performSegueWithIdentifier("DisplayRecent", sender: self)
    }

}