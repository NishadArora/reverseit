import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer
import AVKit
import GoogleMobileAds
import Social

class DisplayViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate , MPMediaPickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource ,GADBannerViewDelegate{
    
    var image                       = UIImage()
    var videoURL                    = NSURL()
    var filePath                    = NSString()
    var player                      = AVPlayer()
    var playerLayer                 = AVPlayerLayer()
    var playerViewController        = AVPlayerViewController()
    let fileManager                 = NSFileManager()
    var latestFilePath              = NSString()
    let date                        = NSDate()
    let calendar                    = NSCalendar.currentCalendar()
    var mediaPicker                 : MPMediaPickerController?
    let audioURL                    = NSURL()
    let identifier                  = "CellIdentifier"
    let blackBackgroundNewHeight    = UIScreen.mainScreen().bounds.size.width - 16
    var newImage                    : UIImage?
    var newVideoURL                 : NSURL?
    var invisibleButton             = UIButton()
    var newHeight                   : CGFloat?
    let screenSize                  = UIScreen.mainScreen().bounds.size.height - 207
    let paths                       = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
    var bannerView                  : GADBannerView!
    
    @IBOutlet weak var imageView                    : UIImageView!
    @IBOutlet weak var previewImage                 : UIImageView!
    @IBOutlet weak var saveButton                   : UIBarButtonItem!
    @IBOutlet weak var collectionView               : UICollectionView!
    @IBOutlet weak var previewImageView             : UIView!
    @IBOutlet weak var imageViewHeight              : NSLayoutConstraint!
    @IBOutlet weak var previewBlackBackgroundView   : UIView!
    @IBOutlet weak var blackBackgroundHeight        : NSLayoutConstraint!
    @IBOutlet weak var imageViewWidth               : NSLayoutConstraint!
    @IBOutlet weak var blackBackgroundWidth         : NSLayoutConstraint!
    @IBOutlet weak var rightArrowWidth              : NSLayoutConstraint!
    @IBOutlet weak var leftArrowWidth               : NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        leftArrowWidth.constant = 0
        
        imageView.image = image
        
        let mainScreenSize                                  = UIScreen.mainScreen().bounds
        let collectionViewFlowLayout                        = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewFlowLayout.itemSize                   = CGSizeMake(mainScreenSize.width / 5.0, mainScreenSize.height / 5.0)
        collectionViewFlowLayout.minimumInteritemSpacing    = 0.0
        collectionViewFlowLayout.minimumLineSpacing         = 0.0
        let sectionInset                                    = mainScreenSize.width / 150.0
        collectionViewFlowLayout.sectionInset               = UIEdgeInsetsMake(0.0, sectionInset, 0.0, sectionInset)
        collectionView.delegate                             = self
        
        let tapGestureRecognizer                            = UITapGestureRecognizer(target:self, action:#selector(DisplayViewController.imageTapped(_:)))
        previewImage.userInteractionEnabled                 = true
        previewImage.addGestureRecognizer(tapGestureRecognizer)
        
        imageView.addSubview(invisibleButton)
        invisibleButton.addTarget(self, action: #selector(DisplayViewController.invisibleButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        ThemeColor()
    }
    
    override func viewDidAppear(animated: Bool) {
        imageView.image = image
        previewImage.hidden = false
        let nsurl = NSUserDefaults.standardUserDefaults()
        if nsurl.URLForKey("VideoNSURL") != nil {
            videoURL = nsurl.URLForKey("VideoNSURL")!
            previewImageFunc()
            if newImage != nil {
                playerLayer.hidden = true
                image = newImage!
                imageView.image = image
            }else{
                playerLayer.hidden = true
                imageView.image = image
            }
        }
        ThemeColor()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewDidDisappear(animated: Bool) {
        playerViewController.view.removeFromSuperview()
        playerLayer.hidden = true
        player.pause()
    }
    
    //  MARK: Save Button Clicked Function
    @IBAction func saveButtonClicked(sender: AnyObject) {
        latestFilePath  = videoURL.path!
        newVideoURL     = videoURL
        
        // Share Sheet Start
        let objectToShare   = [videoURL]
        let activityVC      = UIActivityViewController(activityItems: objectToShare, applicationActivities: nil)
        activityVC.setValue("Video", forKey: "Subject")
        self.presentViewController(activityVC, animated: true, completion: nil)
        
        // Video saving process to recent folder
        let documentsDirectory: AnyObject   = paths[0]
        let dataPath : NSString             = documentsDirectory.stringByAppendingPathComponent("MyFolder")
        let videoData  = NSData(contentsOfURL: videoURL)
        let components = self.calendar.components([.Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let savePath   = dataPath.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds).mov")
        
        if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(latestFilePath as String) {
            do{
                try fileManager.createDirectoryAtPath(dataPath as String, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("Error creating new folder directory in application")
            }
            do {
                try videoData?.writeToFile(savePath, options: NSDataWritingOptions.DataWritingAtomic)
                print("save path ", savePath)
            }catch {
                print("video not saved to path ", savePath)
            }
        }
    }
    
    func invisibleButtonTapped(sender: UIButton!) {
        let playerIsPlaying:Bool = player.rate > 0
        if (playerIsPlaying) {
            player.pause()
        } else {
            player.play()
        }
    }
    
    func video(videoPath: String, didFinishSavingWithError error: NSError, contextInfo info: UnsafeMutablePointer<Void>) {
    }
    
    //  Previrew Image Clicked Function
    
    func imageTapped(img: AnyObject) {
        player               = AVPlayer(URL: videoURL )
        playerLayer          = AVPlayerLayer(player: player)
        playerLayer.frame    = self.imageView.bounds
        previewImage.hidden  = true
        imageView.layer.addSublayer(playerLayer)
        imageView.image = nil
        player.play()
        
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(AudioViewController.playerItemDidReachEnd(_:)),name: AVPlayerItemDidPlayToEndTimeNotification,object: player.currentItem)
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            playerItem.seekToTime(kCMTimeZero)
        }
    }
    
    func previewImageFunc() {
        let asset : AVAsset                             = AVAsset(URL: videoURL)
        let assetImgGenerate : AVAssetImageGenerator    = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time        : CMTime                        = CMTimeMake(10, 10)
        var img         : CGImageRef!
        do {
            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
        } catch  {
        }
        
        let frameImg    = UIImage(CGImage: img)
        imageView.image = frameImg
        image           = frameImg
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell                    = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath)
        cell.layer.cornerRadius     = 20.0
        cell.layer.masksToBounds    = true
        cell.backgroundColor        = UIColor.whiteColor()
        let labelArray              = ["REVERSE","CROP","TRIM","RESIZE","SPEED","ROTATE","AUDIO"]
        let imageArray              = ["reverseIcon" , "cropIcon" , "trimIcon" , "resizeIcon" , "speedIcon" , "rotateIcon","audioIcon"]
        (cell.contentView.viewWithTag(20) as! UILabel).text = labelArray[indexPath.item]
        (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named: "\(imageArray[indexPath.item])")
        (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        return cell
    }
    
    // handle tap events
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        switch(indexPath.item){
        case 0:
            performSegueWithIdentifier("Reverse", sender: self)
        case 1:
            performSegueWithIdentifier("Crop", sender: self)
        case 2:
            performSegueWithIdentifier("Trim", sender: self)
        case 3:
            performSegueWithIdentifier("Resize", sender: self)
        case 4:
            performSegueWithIdentifier("Speed", sender: self)
        case 5:
            performSegueWithIdentifier("Rotate", sender: self)
        default:
            performSegueWithIdentifier("Audio", sender: self)
            break
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier         == "Reverse"{
            let vc                  = segue.destinationViewController as! ReverseViewController
            if newImage != nil {
                vc.imageThumbnail       = newImage!
            } else {
                vc.imageThumbnail       = image
            }
            vc.videoPathURL             = videoURL
        }else if segue.identifier   == "Crop"{
            let vc                  = segue.destinationViewController as! CropViewController
            if newImage != nil {
                vc.imageThumbnail       = newImage!
            } else {
                vc.imageThumbnail       = image
            }
            vc.videoPathURL             = videoURL
        }else if segue.identifier   == "Trim" {
            let vc                  = segue.destinationViewController as! TrimViewController
            vc.videoPathURL             = videoURL
            if newImage != nil {
                vc.imageThumbnail       = newImage!
            } else {
                vc.imageThumbnail       = image
            }
        }else if segue.identifier   == "Resize" {
            let vc                  = segue.destinationViewController as! ReSizeViewContoller
            if newImage != nil {
                vc.imageThumbnail       = newImage!
            } else {
                vc.imageThumbnail       = image
            }
            vc.videoPathURL             = videoURL
        }else if segue.identifier   == "Speed" {
            let vc                  = segue.destinationViewController as! SpeedViewController
            if newImage != nil {
                vc.imageThumbnail       = newImage!
            } else {
                vc.imageThumbnail       = image
            }
            vc.videoPathURL             = videoURL
        }else if segue.identifier   == "Rotate" {
            let vc                  = segue.destinationViewController as! RotateViewContoller
            if newImage != nil {
                vc.imageThumbnail       = newImage!
            } else {
                vc.imageThumbnail       = image
            }
            vc.videoPathURL             = videoURL
        }else if segue.identifier   == "Audio" {
            let vc                  = segue.destinationViewController as! AudioViewController
            if newImage != nil {
                vc.imageThumbnail       = newImage!
            } else {
                vc.imageThumbnail       = image
            }
            vc.videoPathURL             = videoURL
        }
    }
    
    func ThemeColor() {
        self.navigationController?.navigationBarHidden                = false
        self.navigationController?.navigationBar.titleTextAttributes  = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor            = UIColor.whiteColor()
        self.navigationController?.navigationBar.barStyle             = UIBarStyle.Black
        
        navigationController!.navigationBar.barTintColor  = UIColor(red: 130/255 ,green: 0/255 , blue: 0/255 , alpha: 1.0)
        previewImageView.backgroundColor                  = UIColor(red: 223/255 , green: 223/255 , blue: 223/255 , alpha: 1.0)
        collectionView.backgroundColor                    = UIColor(red: 255/255 , green: 255/255 , blue: 255/255 , alpha: 1.0)
        
        let newOldWidth       = image.size.width
        let scaleFactor       = (UIScreen.mainScreen().bounds.size.width - 30)/newOldWidth
        newHeight             = image.size.height*scaleFactor
        invisibleButton.frame = imageView.frame
        
        if newHeight < screenSize {
            imageViewHeight.constant         = newHeight!
            blackBackgroundHeight.constant   = newHeight!
            imageViewWidth.constant          = blackBackgroundNewHeight
            blackBackgroundWidth.constant    = blackBackgroundNewHeight
        } else {
            imageViewHeight.constant         = screenSize
            blackBackgroundHeight.constant   = screenSize
            let sfVideo                      = image.size.height/image.size.width
            imageViewWidth.constant          = screenSize/sfVideo
            blackBackgroundWidth.constant    = screenSize/sfVideo
        }
        
        let origin = CGPointMake(0 , UIScreen.mainScreen().bounds.size.height - (CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height + 120))
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait,origin: origin)
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3589571889245561/6463841756"
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kDFPSimulatorID,"55a52af80cba60b60e3045dfa9bfcd9d","a976aeed5251d82899db95f6bc4ab1a7"]
        bannerView.loadRequest(request)
        view.addSubview(bannerView)
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.item == 5{
            leftArrowWidth.constant = 10
            rightArrowWidth.constant = 10
        }else if indexPath.item == 0{
            leftArrowWidth.constant = 0
            rightArrowWidth.constant = 10
        }else if indexPath.item == 6 {
            rightArrowWidth.constant = 0
            leftArrowWidth.constant = 10
        }
    }
    
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("add appeared")
        
        bannerView.hidden = false
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        bannerView.hidden = true
        print("add failed to appear", error.localizedDescription)
    }
}