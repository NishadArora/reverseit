
import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer
import AVKit
import GoogleMobileAds
import ARSLineProgress

class ReSizeViewContoller: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, MPMediaPickerControllerDelegate,  UICollectionViewDelegate , UICollectionViewDataSource ,GADBannerViewDelegate {
    
    var imageThumbnail              = UIImage()
    var videoPathURL                = NSURL()
    let identifier                  = "CellIdentifier"
    var playerViewController        = AVPlayerViewController()
    var blackBackgroundNewHeight    = UIScreen.mainScreen().bounds.size.width - 16
    var player                      = AVPlayer()
    var playerLayer                 = AVPlayerLayer()
    let finalURL                    = NSURL()
    var latestFilePath              = NSString()
    let date                        = NSDate()
    let calendar                    = NSCalendar.currentCalendar()
    var finalFileURL                = NSURL()
    var playURL                     = NSURL()
    var invisibleButton             = UIButton()
    var newHeight                   : CGFloat?
    let screenSize                  = UIScreen.mainScreen().bounds.size.height - 207
    var bannerView                  : GADBannerView!
    var dataPath                    : String?
    
    @IBOutlet weak var spinnerView          : UIView!
    @IBOutlet weak var collectionView       : UICollectionView!
    @IBOutlet weak var imageView            : UIImageView!
    @IBOutlet weak var backgroundView       : UIView!
    @IBOutlet weak var blackBackgroundView  : UIView!
    @IBOutlet weak var playimageView        : UIImageView!
    @IBOutlet weak var bBViewHeight         : NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight      : NSLayoutConstraint!
    @IBOutlet weak var saveVideo            : UIBarButtonItem!
    @IBOutlet weak var bBViewWidth          : NSLayoutConstraint!
    @IBOutlet weak var imageViewWidth       : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ARSLineProgress.showOnView(spinnerView)
        playURL         = videoPathURL
        imageView.image = imageThumbnail
        
        // collection view setup
        let mainScreenSize                                  = UIScreen.mainScreen().bounds
        let collectionViewFlowLayout                        = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewFlowLayout.itemSize                   = CGSizeMake(mainScreenSize.width / 5.0, mainScreenSize.height / 5.0)
        collectionViewFlowLayout.minimumInteritemSpacing    =  0.0
        collectionViewFlowLayout.minimumLineSpacing         =   0.0
        collectionViewFlowLayout.sectionInset               = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        
        // tap gesture
        let tapGestureRecognizer                = UITapGestureRecognizer(target:self, action:#selector(ReSizeViewContoller.imageTapped))
        playimageView.userInteractionEnabled    = true
        playimageView.addGestureRecognizer(tapGestureRecognizer)
        
        // adding invisible button
        imageView.addSubview(invisibleButton)
        invisibleButton.addTarget(self, action: #selector(ReSizeViewContoller.invisibleButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        ThemeColor()
        getNewVideoOrientation()
    }
    
    override func viewDidAppear(animated: Bool) {
        ThemeColor()
    }
    
    override func viewDidDisappear(animated: Bool) {
        player.pause()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        imageView.reloadInputViews()
        invisibleButton.frame = imageView.bounds
    }
    
    func getNewVideoOrientation() -> UIImageOrientation {
        let asset       = AVAsset(URL: self.videoPathURL)
        let videoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let size        = videoTrack.naturalSize
        let txf         = videoTrack.preferredTransform
        
        if (size.width == txf.tx && size.height == txf.ty) {
            print("***   Video orientation is left")
            return UIImageOrientation.Left
        }else if (txf.tx == 0 && txf.ty == 0) {
            print("***   Video orientation is right")
            return UIImageOrientation.Right
        }else if (txf.tx == 0 && txf.ty == size.width) {
            print("***   Video orientation is dowm")
            return UIImageOrientation.Down
        }else {
            print("***   Video orientation is up")
            return UIImageOrientation.Up
        }
    }

    func resizeVideo75(){
        playerLayer.hidden    = true
        spinnerView.hidden    = false
        playimageView.hidden  = true
        player.pause()

        let asset           = AVURLAsset(URL: videoPathURL)
        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        
        let videoSizeMain           = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        let cropSquare              = CGRectMake(0, 0, videoSizeMain.width  , videoSizeMain.height)
        videoComposition.renderSize = CGSizeMake(cropSquare.size.width , cropSquare.size.height)
        
        let instruction       = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration)
        let layerInstruction  = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        var t2 = CGAffineTransformIdentity
        
        t2 = CGAffineTransformMakeRotation(CGFloat(M_PI*2))
        
        let finalTranform = t2
        layerInstruction.setTransform(finalTranform, atTime: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // save rotated video
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)Resized.mov")
        let finalFileURL2      = NSURL.fileURLWithPath(filePath as String)
        
        let exporter                = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality)!
        exporter.videoComposition   = videoComposition
        exporter.outputURL          = finalFileURL2
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        if NSFileManager.defaultManager().fileExistsAtPath(exporter.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinish(exporter)
                self.checkOrientation(finalFileURL2)
            } )
        }
    }
    
    func resizeVideo50() {
        playerLayer.hidden    = true
        spinnerView.hidden    = false
        playimageView.hidden  = true
        player.pause()
        
        let asset           = AVURLAsset(URL: videoPathURL)
        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        
        let videoSizeMain           = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        let cropSquare              = CGRectMake(0, 0, videoSizeMain.width  , videoSizeMain.height)
        videoComposition.renderSize = CGSizeMake(cropSquare.size.width , cropSquare.size.height)
        
        let instruction       = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration)
        let layerInstruction  = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        var t2 = CGAffineTransformIdentity
        
        t2 = CGAffineTransformMakeRotation(CGFloat(M_PI*2))
        
        let finalTranform = t2
        layerInstruction.setTransform(finalTranform, atTime: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // save rotated video
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)Resized.mov")
        let finalFileURL2      = NSURL.fileURLWithPath(filePath as String)
        
        let exporter                = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetLowQuality)!
        exporter.videoComposition   = videoComposition
        exporter.outputURL          = finalFileURL2
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        if NSFileManager.defaultManager().fileExistsAtPath(exporter.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinish(exporter)
                print("final video url",finalFileURL2)
                self.checkOrientation(finalFileURL2)
            } )
        }
    }
    
//    func resizeVideo25() -> NSURL {
//        playerLayer.hidden    = true
//        spinnerView.hidden    = false
//        playimageView.hidden  = true
//        player.pause()
//        
//        let asset           = AVURLAsset(URL: videoPathURL)
//        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
//        let size            = CGSizeMake(clipVideoTrack.naturalSize.width - 0.25*clipVideoTrack.naturalSize.width , clipVideoTrack.naturalSize.height - 0.25*clipVideoTrack.naturalSize.height)
//        
//        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
//        let hour       = components.hour
//        let minutes    = components.minute
//        let seconds    = components.second
//        
//        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
//        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
//        dataPath                = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)Resized.mov")
//        let dataUrl             = NSURL.fileURLWithPath((dataPath! as String!))
//        if NSFileManager.defaultManager().fileExistsAtPath(dataPath!) {
//            do {
//                try NSFileManager.defaultManager().removeItemAtURL(dataUrl)
//                print("file existed and item removed")
//            } catch _ {
//            }
//        }else {
//            print("file not existed")
//        }
//        let outputUrl       = dataUrl
//        let sourceUrl       = videoPathURL
//        let resizeOperation = ResizeOperation(sourceURL: sourceUrl, outputURL: outputUrl, size: size)
//        resizeOperation.start()
//        print("url of the output video"  , outputUrl)
//        finalFileURL = outputUrl
//        return finalFileURL
//    }
    
    func exportDidFinish(session : AVAssetExportSession) -> Void {
        let outputURL = session.outputURL!
        print(outputURL)
    }
    
    func checkOrientation(videoURL : NSURL){
        if getNewVideoOrientation() == getOldVideoOrientation(videoURL) {
            playURL = videoURL
            self.spinnerView.hidden   = true
            self.playimageView.hidden = false
            self.playerLayer.hidden   = true
            self.imageView.image      = self.imageThumbnail
            self.imageView.reloadInputViews()
        }else {
            rotateVideoLeft(videoURL)
        }
    }
    
    func rotateVideoLeft(url : NSURL){
        let asset           = AVURLAsset(URL: url)
        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        
        let videoSizeMain           = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        let cropSquare              = CGRectMake(0, 0, videoSizeMain.width  , videoSizeMain.height)
        videoComposition.renderSize = CGSizeMake(cropSquare.size.height , cropSquare.size.width)
        
        let instruction       = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration)
        let layerInstruction  = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        var t1 = CGAffineTransformIdentity
        var t2 = CGAffineTransformIdentity
        
        t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height , 0)
        t2 = CGAffineTransformRotate(t1, CGFloat(M_PI_2))
        
        let finalTranform = t2
        
        layerInstruction.setTransform(finalTranform, atTime: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // save rotated video
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)rotatedleft.mov")
        self.latestFilePath    = filePath
        let finalFileURL1 : NSURL!
        finalFileURL1          = NSURL.fileURLWithPath(latestFilePath as String)
        
        let exporter                = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.videoComposition   = videoComposition
        exporter.outputURL          = finalFileURL1
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        if NSFileManager.defaultManager().fileExistsAtPath(exporter.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinishChecking(exporter)
                self.spinnerView.hidden   = true
                self.playimageView.hidden = false
                self.playerLayer.hidden   = true
                self.imageView.image      = self.imageThumbnail
                self.imageView.reloadInputViews()
            } )
        }
    }
    
    func exportDidFinishChecking(session : AVAssetExportSession) -> Void {
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed Exporting the video *")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed")
            print("Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        
        if(session.status == .Completed){
            playURL = session.outputURL!
            print("*** url of the reversed video",playURL)
        }
    }

    func getOldVideoOrientation(url : NSURL) -> UIImageOrientation {
        let asset       = AVAsset(URL: url)
        let videoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let size        = videoTrack.naturalSize
        let txf         = videoTrack.preferredTransform
        
        if (size.width == txf.tx && size.height == txf.ty) {
            print("***   Video orientation is left")
            return UIImageOrientation.Left
        }else if (txf.tx == 0 && txf.ty == 0) {
            print("***   Video orientation is right")
            return UIImageOrientation.Right
        }else if (txf.tx == 0 && txf.ty == size.width) {
            print("***   Video orientation is dowm")
            return UIImageOrientation.Down
        }else {
            print("***   Video orientation is up")
            return UIImageOrientation.Up
        }
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell                 = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath)
        cell.layer.cornerRadius  = 20.0
        cell.layer.masksToBounds = true
        let imageArray           = ["resize50" , "resize25"]
        let labelArray           = ["Medium Resolution","Low Resolution"]
        cell.backgroundColor     = UIColor.whiteColor()
        (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named: "\(imageArray[indexPath.item])")
        (cell.contentView.viewWithTag(20) as! UILabel).text      = labelArray[indexPath.item]
        (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        playerLayer.hidden   = true
        player.pause()
        cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
        
        if indexPath.item       == 0 {
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"resize75_selected")
            resizeVideo75()
            ARSLineProgress.show()
        }else if indexPath.item == 1 {
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"resize50_selected")
            ARSLineProgress.show()
            resizeVideo50()
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        if indexPath.item       == 0{
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"resize50")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
            
        }else if indexPath.item == 1{
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"resize25")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
            
        }
    }
    
    @IBAction func saveVideo(sender: AnyObject) {
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setURL(playURL, forKey: "VideoNSURL")
        navigationController?.popViewControllerAnimated(true)
    }
    
    func ThemeColor() {
        ARSLineProgress.showOnView(spinnerView)
        navigationController!.navigationBar.barTintColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        backgroundView.backgroundColor                   = UIColor(red: 223/255 , green: 223/255 , blue: 223/255 , alpha: 1.0)
        collectionView.backgroundColor                   = UIColor(red: 255/255 , green: 255/255 , blue: 255/255 , alpha: 1.0)
        
        let oldWidth                        = imageThumbnail.size.width
        let scaleFactor                     = (UIScreen.mainScreen().bounds.size.width - 30)/oldWidth
        newHeight                           = imageThumbnail.size.height*scaleFactor
        if newHeight < screenSize {
            imageViewHeight.constant        = newHeight!
            bBViewHeight.constant           = newHeight!
            imageViewWidth.constant         = blackBackgroundNewHeight
            bBViewWidth.constant            = blackBackgroundNewHeight
        } else {
            imageViewHeight.constant        = screenSize
            bBViewHeight.constant           = screenSize
            let sfVideo                     = imageThumbnail.size.height/imageThumbnail.size.width
            imageViewWidth.constant         = screenSize/sfVideo
            bBViewWidth.constant            = screenSize/sfVideo
        }
        
        spinnerView.layer.cornerRadius  = 10.0
        spinnerView.hidden              = true
        
        let origin = CGPointMake(0 , UIScreen.mainScreen().bounds.size.height - (CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height + 120))
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait,origin: origin)
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3589571889245561/6463841756"
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kDFPSimulatorID,"a976aeed5251d82899db95f6bc4ab1a7","55a52af80cba60b60e3045dfa9bfcd9d"]
        bannerView.loadRequest(request)
        view.addSubview(bannerView)
    }
    
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("add appeared")
        
        bannerView.hidden = false
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        bannerView.hidden = true
        print("add failed to appear", error.localizedDescription)
    }
    
    func imageTapped(){
        player               = AVPlayer(URL: playURL)
        playerLayer          = AVPlayerLayer(player: player)
        playerLayer.frame    = self.imageView.bounds
        playimageView.hidden = true
        imageView.layer.addSublayer(playerLayer)
        player.play()
        
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(AudioViewController.playerItemDidReachEnd(_:)),name: AVPlayerItemDidPlayToEndTimeNotification,object: player.currentItem)
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            playerItem.seekToTime(kCMTimeZero)
        }
    }
    
    func invisibleButtonTapped(sender: UIButton!) {
        let playerIsPlaying:Bool = player.rate > 0
        if (playerIsPlaying) {
            player.pause();
        } else {
            player.play();
        }
    }
}