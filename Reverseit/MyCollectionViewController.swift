import UIKit
import AVKit

let reuseIdentifier = "MyCell"

class MyCollectionViewController: UICollectionViewController {
    
    var docDir      : String?
    let fileManager = NSFileManager.defaultManager()
    var videosList  = [String]()
    var dataPath    : String?
    let paths       = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
    var deleteIndex : Int?
    
    @IBOutlet weak var backbtn: UIBarButtonItem!
    //    @IBOutlet weak var deleteVideoButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = UIColor.whiteColor()
        let screenSize                                      = UIScreen.mainScreen().bounds
        let collectionViewFlowLayout                        = collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewFlowLayout.itemSize                   = CGSizeMake((screenSize.width / 4.0)-3 , screenSize.height / 7.0)
        collectionViewFlowLayout.minimumInteritemSpacing    = 1.0
        collectionViewFlowLayout.minimumLineSpacing         = 2.0
        collectionViewFlowLayout.sectionInset               = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        collectionView?.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        ThemeColor()
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let documentsDirectory: AnyObject = paths[0]
        dataPath = documentsDirectory.stringByAppendingPathComponent("MyFolder")
        
        do {
            videosList = try fileManager.contentsOfDirectoryAtPath(dataPath!)
        } catch {
        }
    
        if videosList.count == 0 {
            let alert = UIAlertController(title: "WARNING",message: "No Recent Videos!",preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Done", style: .Default, handler: {action in
                self.dismissViewControllerAnimated(true , completion: nil)
            }))
            self.presentViewController(alert, animated:true, completion:nil)
            
        }
        return self.videosList.count
    }
    
    @IBAction func back(sender: AnyObject) {
        self.dismissViewControllerAnimated(true , completion: nil)
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MyCollectionViewCell
        deleteIndex = indexPath.item
        (cell.contentView.viewWithTag(50) as! UIImageView).image = getImage(dataPath!+"/"+videosList[indexPath.item])
        return cell
    }
    
    func getImage(pathString : String) -> UIImage {
        
        let path : NSURL = NSURL.fileURLWithPath(pathString, isDirectory: true)
        let asset : AVAsset                             = AVAsset(URL: path)
        let assetImgGenerate : AVAssetImageGenerator    = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time        : CMTime                        = CMTimeMake(10, 10)
        var img         : CGImageRef!
        do {
            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
        } catch  {
        }
        
        let frameImg    : UIImage   = UIImage(CGImage: img)
        return frameImg
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let path        = NSURL.fileURLWithPath((dataPath!+"/"+videosList[indexPath.item]))
        let playerVC    = AVPlayerViewController()
        let player      = AVPlayer(URL: path)
        playerVC.player = player
        self.presentViewController(playerVC, animated: true, completion: {
            player.play()
        })
    }
    
    func ThemeColor() {
        navigationController!.navigationBar.barTintColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        collectionView!.backgroundColor                  = UIColor(red: 255/255 , green: 255/255 , blue: 255/255 , alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes    = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        navigationController?.navigationBar.tintColor              = UIColor.whiteColor()
        navigationController?.navigationBar.barStyle               = UIBarStyle.Black
    }
    
}

//    @IBAction func deleteButtonAction(sender: AnyObject) {
//        do {
//            try fileManager.removeItemAtPath(dataPath!+"/"+videosList![deleteIndex!+1])
//            print("item deleted")
//        }catch{
//            print("item not deleted")
//        }
//    }