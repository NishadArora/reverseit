//
//  previewViewController.swift
//  Reverseit
//
//  Created by Nishad Arora on 08/01/16.
//  Copyright © 2016 Nishad Arora. All rights reserved.
//

import UIKit
import AVKit

class previewViewController: AVPlayerViewController {

    let player = AVPlayer(URL: url)
    let playerController = AVPlayerViewController()
    
    playerController.player = player
    self.addChildViewController(playerController)
    self.view.addSubview(playerController.view)
    playerController.view.frame = self.view.frame
    
    player.play()
    
}
