import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer
import AVKit
import GoogleMobileAds
import ARSLineProgress

class TrimViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate,  ICGVideoTrimmerDelegate , GADBannerViewDelegate {
    
    var imageThumbnail              = UIImage()
    var videoPathURL                = NSURL()
    var isPlaying                   = Bool()
    var player                      : AVPlayer?
    var playerItem                  : AVPlayerItem?
    var playerLayer                 : AVPlayerLayer?
    var playbackTimeCheckerTimer    : NSTimer?
    var videoPlaybackPosition       = CGFloat()
    let date                        = NSDate()
    let calendar                    = NSCalendar.currentCalendar()
    var trimmmedVideoPath           : String?
    var trimmmedVideoURL            : NSURL?
    var tempVideoPath               : NSString?
    var exportSession               : AVAssetExportSession?
    var asset                       : AVAsset?
    var startTime                   = CGFloat()
    var stopTime                    = CGFloat()
    var playerViewController        = AVPlayerViewController()
    var blackBackgroundNewHeight    = UIScreen.mainScreen().bounds.size.width - 16
    var playVideo                   : NSURL?
    var newVidUrl                   : NSURL?
    var newHeight                   : CGFloat?
    let screenSize                  = UIScreen.mainScreen().bounds.size.height - 227
    var bannerView                  : GADBannerView!
    
    @IBOutlet weak var spinnerView              : UIView!
    @IBOutlet weak var backgroundView           : UIView!
    @IBOutlet weak var blackBackgroundView      : UIView!
    @IBOutlet weak var trimView                 : ICGVideoTrimmerView!
    @IBOutlet weak var videoLayer               : UIView!
    @IBOutlet weak var bBHeight                 : NSLayoutConstraint!
    @IBOutlet weak var BbWidth                  : NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight          : NSLayoutConstraint!
    @IBOutlet weak var imageViewWidth           : NSLayoutConstraint!
    @IBOutlet weak var saveVideo                : UIBarButtonItem!
    @IBOutlet weak var imageView                : UIImageView!
    @IBOutlet weak var playImageView            : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ARSLineProgress.showOnView(spinnerView)
        ThemeColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        // setting up video player
        asset                           = AVAsset(URL: videoPathURL)
        let item                        = AVPlayerItem(asset: asset!)
        player                          = AVPlayer(playerItem: item)
        playerViewController.player     = player
        playerViewController.view.frame = blackBackgroundView.bounds
        playerLayer?.frame              = blackBackgroundView!.bounds
        self.blackBackgroundView.addSubview(playerViewController.view)
        player?.pause()
        
        player!.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(AudioViewController.playerItemDidReachEnd(_:)),name: AVPlayerItemDidPlayToEndTimeNotification,object: player!.currentItem)
    }

    override func viewDidAppear(animated: Bool) {
        playVid()
        ARSLineProgress.showOnView(spinnerView)
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            playerItem.seekToTime(kCMTimeZero)
        }
    }
    
    func trimmerView(trimmerView: ICGVideoTrimmerView!, didChangeLeftPosition startTime: CGFloat, rightPosition endTime: CGFloat) {
        if(startTime != self.startTime){
            self.seekVideoToPas(startTime)
        }
        self.startTime = startTime
        self.stopTime  = endTime
    }
    
    func playVid() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TrimViewController.tapOnVideoLayer(_:)))
        self.blackBackgroundView.addGestureRecognizer(tap)
        self.tapOnVideoLayer(tap)
        
        self.trimView.themeColor        = UIColor.yellowColor()
        self.trimView.asset             = asset
        self.trimView.showsRulerView    = true
        self.trimView.trackerColor      = UIColor.clearColor()
        self.trimView.delegate          = self
        self.trimView.resetSubviews()
    }
    
    func trimVideo() {
        let compatiblePresents : NSArray        = AVAssetExportSession.exportPresetsCompatibleWithAsset(self.asset!)
        
        if compatiblePresents.containsObject(AVAssetExportPresetHighestQuality){
            self.exportSession                  = AVAssetExportSession(asset: asset!, presetName: AVAssetExportPresetPassthrough)
            let paths : NSArray                 = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
            let documentsDirectory : NSString   = paths.objectAtIndex(0) as! NSString
            let components      = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
            let hour            = components.hour
            let minutes         = components.minute
            let seconds         = components.second
            trimmmedVideoPath   = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)t.mov")
            trimmmedVideoURL    = NSURL.fileURLWithPath(trimmmedVideoPath!)
            
            self.exportSession?.outputURL       = trimmmedVideoURL
            self.exportSession?.outputFileType  = AVFileTypeQuickTimeMovie
            
            let start       = CMTimeMakeWithSeconds(Float64(self.startTime), self.asset!.duration.timescale)
            let duration    = CMTimeMakeWithSeconds(Float64(self.stopTime - self.startTime), self.asset!.duration.timescale)
            print("***start time = \(startTime) , stop time = \(stopTime)***")
            let range                       = CMTimeRangeMake(start, duration)
            self.exportSession?.timeRange   = range
            
            self.exportSession?.exportAsynchronouslyWithCompletionHandler { () -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.newVidUrl = self.exportDidFinish(self.exportSession!)
                    self.newVidUrl = self.playVideo
                    self.navigationController?.popViewControllerAnimated(true)
                })
            }
        }
    }
    
    func exportDidFinish(session : AVAssetExportSession) -> NSURL {
        print("The Export DidFinish Function Called")
        print("The AVAssetExportSessionState : \(session.status)")
        
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed")
            print("Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        
        let composition     = AVMutableComposition()
        let exporter        = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = trimmmedVideoURL
        print("movie url is :::::::::::::::::::>\(trimmmedVideoURL)")
        self.spinnerView.hidden = true
        self.playImageView.hidden = false
        return trimmmedVideoURL!

    }
    
    func tapOnVideoLayer(tap : UITapGestureRecognizer) {
        if (self.isPlaying) {
            self.player!.pause()
            self.stopPlaybackTimeChecker()
        } else {
            self.startPlaybackTimeChecker()
        }
        self.isPlaying = true
        self.trimView.hideTracker(self.isPlaying)
    }
    
    func stopPlaybackTimeChecker() {
        if ((self.playbackTimeCheckerTimer)     != nil){
            self.playbackTimeCheckerTimer!.invalidate()
            self.playbackTimeCheckerTimer       = nil
        }
    }
    
    func startPlaybackTimeChecker() {
        self.stopPlaybackTimeChecker()
        self.playbackTimeCheckerTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(TrimViewController.onPlaybackTimeCheckerTimer), userInfo: nil, repeats: true)
    }
    
    func onPlaybackTimeCheckerTimer() {
        self.videoPlaybackPosition      = CGFloat(CMTimeGetSeconds(self.player!.currentTime()))
        self.trimView.seekToTime(videoPlaybackPosition)
        if (self.videoPlaybackPosition >= self.stopTime) {
            self.videoPlaybackPosition  = self.startTime
            self.seekVideoToPas(self.startTime)
            self.trimView.seekToTime(startTime)
        }
    }
    
    @IBAction func saveVideo(sender: AnyObject) {
        playVideoClick()
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setURL(trimmmedVideoURL, forKey: "VideoNSURL")
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func seekVideoToPas(pos : CGFloat) -> Void {
        self.videoPlaybackPosition  = pos
        let time : CMTime           = CMTimeMakeWithSeconds(Float64(self.videoPlaybackPosition), self.player!.currentTime().timescale)
        self.player!.seekToTime(time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero )
    }
    
    func ThemeColor() {
        imageView.hidden     = true
        playImageView.hidden = true
        
        // Theme
        navigationController!.navigationBar.barTintColor    = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        backgroundView.backgroundColor                      = UIColor(red: 255/255 , green: 255/255 , blue: 255/255 , alpha: 1.0)
        
        // setting Image view size
        let oldWidth                = imageThumbnail.size.width
        let scaleFactor             = (UIScreen.mainScreen().bounds.size.width - 30)/oldWidth
        newHeight               = imageThumbnail.size.height*scaleFactor
        if newHeight < screenSize {
            imageViewHeight.constant                        = newHeight!
            bBHeight.constant                               = newHeight!
            imageViewWidth.constant                         = blackBackgroundNewHeight
            BbWidth.constant                                = blackBackgroundNewHeight

        } else {
            imageViewHeight.constant                        = screenSize
            bBHeight.constant                               = screenSize
            let sfVideo                                     = imageThumbnail.size.height/imageThumbnail.size.width
            imageViewWidth.constant                         = screenSize/sfVideo
            BbWidth.constant                                = screenSize/sfVideo
        }
        
        spinnerView.hidden = true
        spinnerView.layer.cornerRadius  = 10.0
        
        let origin = CGPointMake(0 , UIScreen.mainScreen().bounds.size.height - (CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height + 135))
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait,origin: origin)
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3589571889245561/6463841756"
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kDFPSimulatorID,"a976aeed5251d82899db95f6bc4ab1a7","55a52af80cba60b60e3045dfa9bfcd9d"]
        bannerView.loadRequest(request)
        view.addSubview(bannerView)
    }
    
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("add appeared")
        
        bannerView.hidden = false
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        bannerView.hidden = true
        print("add failed to appear", error.localizedDescription)
    }
    
    func cancelViewClicked() {
        videoLayer.hidden   = true
    }
    
    // Saving Handled
    func playVideoClick() {
        playerViewController.view.removeFromSuperview()
        player?.pause()
        print("Tick Tap called")
        blackBackgroundView.hidden  = false
        videoLayer.hidden           = true
        imageView.hidden            = false
        imageView.image             = imageThumbnail
        playImageView.hidden        = true
        spinnerView.hidden          = false
        trimVideo()
    }

}