import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer
import AVKit
import AssetsLibrary
import GoogleMobileAds
import ARSLineProgress

class AudioViewController:
UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,  UICollectionViewDelegate , UICollectionViewDataSource ,GADBannerViewDelegate , RETrimControlDelegate {
    
    var imageThumbnail              = UIImage()
    var videoPathURL                = NSURL()
    var playURL                     = NSURL()
    let identifier                  = "CellIdentifier"
    var latestFilePath              = NSString()
    let date                        = NSDate()
    let calendar                    = NSCalendar.currentCalendar()
    var videoWithAudioURL           : NSURL?
    var videoWithoutAudioURL        : NSURL?
    var mergedVideoURL2             : NSURL?
    var outputUrl                   : NSURL?
    var filePath                    : NSString?
    var path                        : NSString?
    var player                      = AVPlayer()
    var revOriVid                   : NSURL?
    var oriRevVid                   : NSURL?
    var playVid                     : NSURL?
    var playerViewController        = AVPlayerViewController()
    var blackBackgroundNewHeight    = UIScreen.mainScreen().bounds.size.width - 16
    var libraryFolder               = ALAssetsLibrary()
    var mediaPicker                 = MPMediaPickerController(mediaTypes: MPMediaType.Music)
    var selectedSong                : MPMediaItemCollection?
    var songUrl                     : NSURL?
    var trimmmedVideoPath           : NSString?
    var invisibleButton             = UIButton()
    var newHeight                   : CGFloat?
    let screenSize                  = UIScreen.mainScreen().bounds.size.height - 207
    var bannerView                  : GADBannerView!
    var audioURL                    : NSURL?
    var playerLayer                 = AVPlayerLayer()
    var reader                      : AVAssetReader!
    var writer                      : AVAssetWriter!
    var audioPlayer                 = AVAudioPlayer()
    var startTime                   : Int64!
    var endTime                     : Int64!
    var videoWithNoAudioURL         : NSURL?
    
    @IBOutlet weak var backgroundView       : UIView!
    @IBOutlet weak var blackBackgroundView  : UIView!
    @IBOutlet weak var imageView            : UIImageView!
    @IBOutlet weak var collectionView       : UICollectionView!
    @IBOutlet weak var imageViewHeight      : NSLayoutConstraint!
    @IBOutlet weak var bBViewHeight         : NSLayoutConstraint!
    @IBOutlet weak var playimageView        : UIImageView!
    @IBOutlet weak var saveVideo            : UIBarButtonItem!
    @IBOutlet weak var spinnerView          : UIView!
    @IBOutlet weak var bBViewWidth          : NSLayoutConstraint!
    @IBOutlet weak var imageViewWidth       : NSLayoutConstraint!
    @IBOutlet weak var trimView             : UIView!
    @IBOutlet weak var applyMusicBtn        : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ARSLineProgress.showOnView(spinnerView)
        getNewVideoOrientation()
        playVid         = self.videoPathURL
        imageView.image = imageThumbnail
        trimView.hidden = true
        applyMusicBtn.hidden = true
        
        mediaPicker.allowsPickingMultipleItems  = false
        mediaPicker.delegate                    = self
        mediaPicker.prompt                      = "Select a song for video"
        
        let mainScreenSize                                  = UIScreen.mainScreen().bounds
        let collectionViewFlowLayout                        = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewFlowLayout.itemSize                   = CGSizeMake(mainScreenSize.width / 5.0, mainScreenSize.height / 5.0)
        collectionViewFlowLayout.minimumInteritemSpacing    = 0.0
        collectionViewFlowLayout.minimumLineSpacing         = 0.0
        collectionViewFlowLayout.sectionInset               = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        
        // PLAY BUTTON ADDED
        let tapGestureRecognizer             = UITapGestureRecognizer(target:self, action:#selector(AudioViewController.imageTapped))
        playimageView.userInteractionEnabled = true
        playimageView.addGestureRecognizer(tapGestureRecognizer)
        
        // INVISIBLE PAUSE BUTTON ADDED
        imageView.addSubview(invisibleButton)
        invisibleButton.addTarget(self, action: #selector(AudioViewController.invisibleButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        // TRIM VIEW ADDED
        ThemeColor()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated);
        collectionView.reloadData();
    }
    
    override func viewDidDisappear(animated: Bool) {
        playerViewController.view.removeFromSuperview()
        player.pause()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        invisibleButton.frame = imageView.bounds
    }
    
    func invisibleButtonTapped(sender: UIButton!) {
        print("invisible button tapped")
        if player.rate != 0 {
            player.pause()
        } else {
            player.play()
        }
    }
    
    func getNewVideoOrientation() -> UIImageOrientation {
        let asset       = AVAsset(URL: self.videoPathURL)
        let videoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let size        = videoTrack.naturalSize
        let txf         = videoTrack.preferredTransform
        
        if (size.width == txf.tx && size.height == txf.ty) {
            print("***   Video orientation is left")
            return UIImageOrientation.Left
        }else if (txf.tx == 0 && txf.ty == 0) {
            print("***   Video orientation is right")
            return UIImageOrientation.Right
        }else if (txf.tx == 0 && txf.ty == size.width) {
            print("***   Video orientation is dowm")
            return UIImageOrientation.Down
        }else {
            print("***   Video orientation is up")
            return UIImageOrientation.Up
        }
    }
    
    func trimmer(length:Int) {
        applyMusicBtn.hidden = false
        trimView.hidden = false
        let trimControl : RETrimControl = RETrimControl(frame: CGRectMake(0, (UIScreen.mainScreen().bounds.size.height/2 - 28), UIScreen.mainScreen().bounds.size.width, 28))
        trimControl.length              = length
        trimControl.delegate            = self
        trimView.addSubview(trimControl)
    }
    
    func trimControl(trimControl: RETrimControl!, didChangeLeftValue leftValue: CGFloat, rightValue: CGFloat) {
        print("left",leftValue,"right",rightValue)
        
        do{
            audioPlayer = try AVAudioPlayer(contentsOfURL: songUrl!)
        }catch{
            print("error playing song")
        }
        audioPlayer.prepareToPlay()
        audioPlayer.currentTime = (Double(leftValue))
        audioPlayer.play()
        print("starting timer is  :",leftValue)
        startTime = Int64(leftValue)
        endTime   = Int64(rightValue)
    }
    
    @IBAction func applyMusic(sender: AnyObject) {
        applyMusicBtn.hidden = true
        trimView.hidden      = true
        spinnerView.hidden   = true
        audioPlayer.stop()
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let paths : NSArray                 = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory : NSString   = paths.objectAtIndex(0) as! NSString
        let audioFilePath                   = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds).mov")
        let audioFileURL                    = NSURL.fileURLWithPath(audioFilePath)
        
        let audioAsset : AVURLAsset = AVURLAsset(URL: songUrl!, options: nil)
        let exporter                = AVAssetExportSession(asset: audioAsset, presetName: AVAssetExportPresetAppleM4A)
        exporter?.outputFileType    = AVFileTypeAppleM4A
        exporter?.outputURL         = NSURL.fileURLWithPath(audioFileURL.path!)
        
        let start           = CMTimeMake(startTime, 1)
        let end             = CMTimeMake(endTime, 1)
        let exportTimeRange = CMTimeRangeFromTimeToTime(start, end)
        exporter?.timeRange = exportTimeRange
        
        exporter?.exportAsynchronouslyWithCompletionHandler({
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinishSession(exporter!)
            })
        })
    }
    
    func exportDidFinishSession(session : AVAssetExportSession) -> Void{
        switch session.status {
        case  AVAssetExportSessionStatus.Failed:
            print("export failed \(session.error)")
        case AVAssetExportSessionStatus.Cancelled:
            print("export cancelled \(session.error)")
        case AVAssetExportSessionStatus.Completed:
            print("export completed")
        default:
            print("export default")
        }
        if session.status == AVAssetExportSessionStatus.Completed{
            VideoWithAudio(session.outputURL!)
            spinnerView.hidden = false
        }
    }
    
    //     MARK: REVERSE VIDEO WITH AUDIO FUNCTION
    func VideoWithAudio(audioURL : NSURL){
        audioPlayer.stop()
        let outputURL : NSURL   = videoPathURL
        let composition         = AVMutableComposition()
        let trackVideo          = composition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: CMPersistentTrackID())
        let trackAudio          = composition.addMutableTrackWithMediaType(AVMediaTypeAudio, preferredTrackID: CMPersistentTrackID())
        var insertTime          = kCMTimeZero
        let sourceVideoAsset    = AVURLAsset(URL: outputURL, options: nil)
        let sourceAudioAsset    = AVURLAsset(URL: audioURL, options: nil)
        let tracks              = sourceVideoAsset.tracksWithMediaType(AVMediaTypeVideo)
        let audios              = sourceAudioAsset.tracksWithMediaType(AVMediaTypeAudio)
        
        if tracks.count > 0{
            
            let assetVideoTrack:AVAssetTrack = tracks[0]
            do {
                try trackVideo.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceVideoAsset.duration), ofTrack: assetVideoTrack, atTime: insertTime)
            } catch _ {
            }
            let assetTrackAudio:AVAssetTrack = audios[0]
            do {
                try trackAudio.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceVideoAsset.duration), ofTrack: assetTrackAudio, atTime: insertTime)
            } catch _ {
            }
            insertTime = CMTimeAdd(insertTime, sourceVideoAsset.duration)
        }
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let paths : NSArray                 = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory : NSString   = paths.objectAtIndex(0) as! NSString
        self.filePath                       = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds).mov")
        
        let exporter        = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = NSURL.fileURLWithPath(filePath as! String)
        
        if NSFileManager.defaultManager().fileExistsAtPath(exporter!.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter!.outputURL!)
            } catch _ {
            }
        }
        
        videoWithAudioURL           = exporter!.outputURL
        exporter!.outputFileType    = AVFileTypeMPEG4
        exporter!.exportAsynchronouslyWithCompletionHandler({
            switch exporter!.status{
            case  AVAssetExportSessionStatus.Failed:
                print("failed \(exporter!.error)")
            case AVAssetExportSessionStatus.Cancelled:
                print("cancelled \(exporter!.error)")
            default:
                print("complete")
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.checkOrientation(self.videoWithAudioURL!)
            } )
        } )
    }
    
    //     MARK: REVERSE VIDEO WITH NO AUDIO FUNCTION
    func VideoWithNoAudio(){
        let outputURL : NSURL   = videoPathURL
        let composition         = AVMutableComposition()
        let trackVideo          = composition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: CMPersistentTrackID())
        var insertTime          = kCMTimeZero
        let sourceVideoAsset    = AVURLAsset(URL: outputURL, options: nil)
        let tracks              = sourceVideoAsset.tracksWithMediaType(AVMediaTypeVideo)
        
        if tracks.count > 0{
            let assetTrack:AVAssetTrack = tracks[0]
            do {
                try trackVideo.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceVideoAsset.duration), ofTrack: assetTrack, atTime: insertTime)
            } catch _ {
            }
            insertTime = CMTimeAdd(insertTime, sourceVideoAsset.duration)
        }
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray                 = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory : NSString   = paths.objectAtIndex(0) as! NSString
        self.filePath                       = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)a.mov")
        let exporter        = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = NSURL.fileURLWithPath(filePath as! String)
        
        if NSFileManager.defaultManager().fileExistsAtPath(exporter!.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter!.outputURL!)
            } catch _ {
            }
        }
        
        videoWithoutAudioURL        = exporter!.outputURL
        exporter!.outputFileType    = AVFileTypeMPEG4
        exporter!.exportAsynchronouslyWithCompletionHandler({
            switch exporter!.status{
            case  AVAssetExportSessionStatus.Failed:
                print("failed \(exporter!.error)")
            case AVAssetExportSessionStatus.Cancelled:
                print("cancelled \(exporter!.error)")
            default:
                print("complete")
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.checkOrientation(self.videoWithoutAudioURL!)
            } )
        })
    }
    
    func audioFromVideo() {
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let audioAsset          = AVURLAsset(URL: self.videoPathURL)
        let startTime           = kCMTimeZero
        let endTime             = audioAsset.duration
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        let audioPath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)audio.M4A")
        audioURL                = NSURL.fileURLWithPath(audioPath as String)
        let timeRange           = CMTimeRangeMake(startTime, endTime)
        
        let exporter : AVAssetExportSession  = AVAssetExportSession(asset: audioAsset, presetName: AVAssetExportPresetAppleM4A)!
        exporter.outputURL                   = audioURL
        exporter.timeRange                   = timeRange
        exporter.outputFileType              = AVFileTypeAppleM4A
        exporter.shouldOptimizeForNetworkUse = true
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinishAudio(exporter)
            } )
        }
    }
    
    func exportDidFinishAudio(session : AVAssetExportSession){
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed - Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        if(session.status == .Completed){
            audioURL       = session.outputURL!
            reversedAudio(audioURL!)
        }else {
            print("*** ERROR : Audio File Not Present In The Video **")
            
            let alert = UIAlertController(title: "Warning!",message: "Audio not present in this video",preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                self.playVid               = self.videoPathURL
                self.spinnerView.hidden    = true
                self.playimageView.hidden  = false
                self.playerLayer.hidden    = true
                self.imageView.image       = self.imageThumbnail
            })
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func reversedAudio(url : NSURL) {
        let asset = AVURLAsset(URL: audioURL!)
        do{
            reader = try AVAssetReader(asset: asset)
        }catch{
            print("video unable to read")
        }
        let audioTrack          = asset.tracksWithMediaType(AVMediaTypeAudio)[0]
        let audioReadSetting    = [AVFormatIDKey: Int(kAudioFormatLinearPCM)]
        let readerOutput        = AVAssetReaderTrackOutput(track: audioTrack, outputSettings: audioReadSetting)
        reader.addOutput(readerOutput)
        reader.startReading()
        
        let outputSettings      = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVSampleRateKey: 44100.0,AVNumberOfChannelsKey: 2 as NSNumber, AVEncoderBitRateKey: 128000, AVChannelLayoutKey : NSData()]
        let writerInput         = AVAssetWriterInput(mediaType: AVMediaTypeAudio, outputSettings: outputSettings)
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        let components          = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour                = components.hour
        let minutes             = components.minute
        let seconds             = components.second
        let exportPath          = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)reversedAudio.m4a")
        let exportURL           = NSURL.fileURLWithPath(exportPath)
        
        do{
            writer = try AVAssetWriter(URL: exportURL, fileType: AVFileTypeAppleM4A)
        }catch{
            print("video unable to read")
        }
        writerInput.expectsMediaDataInRealTime = false
        writer.shouldOptimizeForNetworkUse = true
        writer.addInput(writerInput)
        writer.startWriting()
        writer.startSessionAtSourceTime(kCMTimeZero)
        
        let samples = NSMutableArray()
        while let sample : CMSampleBufferRef = readerOutput.copyNextSampleBuffer() {
            samples.addObject(sample)
        }
        let reversedSamples : NSArray = samples.reverseObjectEnumerator().allObjects
        
        for  reversedSample in reversedSamples {
            if writerInput.readyForMoreMediaData {
                writerInput.appendSampleBuffer(reversedSample as! CMSampleBufferRef)
            }else{
                NSThread.sleepForTimeInterval(0.05)
            }
        }
        writerInput.markAsFinished()
        let queue : dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
        dispatch_async(queue) {
            self.writer.finishWritingWithCompletionHandler({
                self.audioToVideo(exportURL)
            })
        }
    }
    
    func checkOrientation(videoURL : NSURL){
        if getNewVideoOrientation() == getOldVideoOrientation(videoURL) {
            playVid = videoURL
            print("play video url ",videoURL)
            self.spinnerView.hidden   = true
            self.playimageView.hidden = false
            self.playerLayer.hidden   = true
            self.imageView.image      = self.imageThumbnail
            self.imageView.reloadInputViews()
        }else {
            rotateVideoLeft(videoURL)
        }
    }
    
    func getOldVideoOrientation(url : NSURL) -> UIImageOrientation {
        let asset       = AVAsset(URL: url)
        let videoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let size        = videoTrack.naturalSize
        let txf         = videoTrack.preferredTransform
        
        if (size.width == txf.tx && size.height == txf.ty) {
            print("***   Video orientation is left")
            return UIImageOrientation.Left
        }else if (txf.tx == 0 && txf.ty == 0) {
            print("***   Video orientation is right")
            return UIImageOrientation.Right
        }else if (txf.tx == 0 && txf.ty == size.width) {
            print("***   Video orientation is dowm")
            return UIImageOrientation.Down
        }else {
            print("***   Video orientation is up")
            return UIImageOrientation.Up
        }
    }
    
    func rotateVideoLeft(url : NSURL){
        let asset           = AVURLAsset(URL: url)
        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        
        let videoSizeMain           = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        let cropSquare              = CGRectMake(0, 0, videoSizeMain.width  , videoSizeMain.height)
        videoComposition.renderSize = CGSizeMake(cropSquare.size.height , cropSquare.size.width)
        
        let instruction       = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration)
        let layerInstruction  = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        var t1 = CGAffineTransformIdentity
        var t2 = CGAffineTransformIdentity
        
        t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height , 0)
        t2 = CGAffineTransformRotate(t1, CGFloat(M_PI_2))
        
        let finalTranform = t2
        
        layerInstruction.setTransform(finalTranform, atTime: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // save rotated video
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)rotatedleft.mov")
        self.latestFilePath    = filePath
        let finalFileURL1 : NSURL!
        finalFileURL1          = NSURL.fileURLWithPath(latestFilePath as String)
        
        let exporter                = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.videoComposition   = videoComposition
        exporter.outputURL          = finalFileURL1
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        if NSFileManager.defaultManager().fileExistsAtPath(exporter.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinishChecking(exporter)
                self.spinnerView.hidden   = true
                self.playimageView.hidden = false
                self.playerLayer.hidden   = true
                self.imageView.image      = self.imageThumbnail
                self.imageView.reloadInputViews()
            } )
        }
    }
    
    func exportDidFinishChecking(session : AVAssetExportSession) -> Void {
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed Exporting the video *")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed")
            print("Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        
        if(session.status == .Completed){
            playVid = session.outputURL!
            print("*** url of the reversed video",playVid)
        }
    }
    
    func audioToVideo(url : NSURL) {
        let composition       = AVMutableComposition()
        let trackVideo        = composition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: CMPersistentTrackID())
        let trackAudio        = composition.addMutableTrackWithMediaType(AVMediaTypeAudio, preferredTrackID: CMPersistentTrackID())
        var insertTime        = kCMTimeZero
        let sourceVideoAsset  = AVURLAsset(URL: videoPathURL, options: nil)
        let sourceAudioAsset  = AVURLAsset(URL: url, options: nil)
        let tracks            = sourceVideoAsset.tracksWithMediaType(AVMediaTypeVideo)
        var audios            = sourceAudioAsset.tracksWithMediaType(AVMediaTypeAudio)
        
        if tracks.count > 0 {
            let assetTrack:AVAssetTrack     = tracks[0]
            do {
                try trackVideo.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceVideoAsset.duration), ofTrack: assetTrack, atTime: insertTime)
            } catch _ {
            }
            if audios.count > 0 {
                let assetTrackAudio:AVAssetTrack = audios[0]
                do {
                    try trackAudio.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceVideoAsset.duration), ofTrack: assetTrackAudio, atTime: insertTime)
                } catch _ {
                }
            } else {
                insertTime = CMTimeAdd(insertTime, sourceVideoAsset.duration)
            }
        }
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        self.filePath          = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds).mov")
        
        let exporter           = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL    = NSURL.fileURLWithPath(filePath as! String)
        
        if NSFileManager.defaultManager().fileExistsAtPath(exporter!.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter!.outputURL!)
            } catch _ {
            }
        }
        
        videoWithAudioURL           = exporter!.outputURL
        exporter!.outputFileType    = AVFileTypeMPEG4
        exporter!.exportAsynchronouslyWithCompletionHandler({
            switch exporter!.status{
            case  AVAssetExportSessionStatus.Failed:
                print("failed \(exporter!.error)")
            case AVAssetExportSessionStatus.Cancelled:
                print("cancelled \(exporter!.error)")
            default:
                print("complete")
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.checkOrientation(self.videoWithAudioURL!)
            } )
        } )
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell                    = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath)
        cell.layer.cornerRadius     = 20.0
        cell.layer.masksToBounds    = true
        let imageArray              = ["addVolumeGreyIcon" , "noVolumeGreyIcon","reverseAudioGrey","originalAudioGrey"]
        (cell.contentView.viewWithTag(10) as! UIImageView).image    = UIImage(named: "\(imageArray[indexPath.item])")
        let labelArray              = ["ADD AUDIO","REMOVE AUDIO","REVERSE AUDIO","ORIGINAL AUDIO"]
        (cell.contentView.viewWithTag(20) as! UILabel).text         = labelArray[indexPath.item]
        (cell.contentView.viewWithTag(20) as! UILabel).textColor    = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        cell.backgroundColor        = UIColor.whiteColor()
        
        return cell
    }
    
    // cell did select function called
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell : UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        if indexPath.item == 0 {
            playerViewController.view.removeFromSuperview()
            player.pause()
            
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"addVolumeWhiteIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            ARSLineProgress.show()
            self.spinnerView.hidden    = false
            self.playimageView.hidden  = true
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.presentViewController(self.mediaPicker, animated: true, completion: { () -> Void in
                })
            } )
        }else if indexPath.item == 1 {
            playerViewController.view.removeFromSuperview()
            player.pause()
            
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"noVolumeWhiteIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            ARSLineProgress.show()
            self.spinnerView.hidden    = false
            self.playimageView.hidden  = true
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.VideoWithNoAudio()
            } )
        }else if indexPath.item == 2 {
            playerViewController.view.removeFromSuperview()
            player.pause()
            
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"reverseAudioWhite")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            ARSLineProgress.show()
            self.spinnerView.hidden    = false
            self.playimageView.hidden  = true
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.audioFromVideo()
            } )
        }else if indexPath.item == 3 {
            playerViewController.view.removeFromSuperview()
            player.pause()
            ARSLineProgress.show()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"originalAudioWhite")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            
            playimageView.hidden  = false
            imageView.image = imageThumbnail
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.playVid = self.videoPathURL
            } )
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell : UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        playerViewController.view.removeFromSuperview()
        
        if indexPath.item == 0 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"addVolumeGreyIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        } else if indexPath.item == 1 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"noVolumeGreyIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        }else if indexPath.item == 2 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"reverseAudioGrey")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        }else if indexPath.item == 3 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"originalAudioGrey")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        }
    }
    
    func ThemeColor() {
        navigationController!.navigationBar.barTintColor    = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        backgroundView.backgroundColor                      = UIColor(red: 223/255 , green: 223/255 , blue: 223/255 , alpha: 1.0)
        collectionView.backgroundColor                      = UIColor(red: 255/255 , green: 255/255 , blue: 255/255 , alpha: 1.0)
        
        let oldWidth    = imageThumbnail.size.width
        let scaleFactor = (UIScreen.mainScreen().bounds.size.width - 30)/oldWidth
        newHeight       = imageThumbnail.size.height*scaleFactor
        spinnerView.layer.cornerRadius  = 10.0
        
        if newHeight < screenSize {
            imageViewHeight.constant   = newHeight!
            bBViewHeight.constant      = newHeight!
            imageViewWidth.constant    = blackBackgroundNewHeight
            bBViewWidth.constant       = blackBackgroundNewHeight
        } else {
            imageViewHeight.constant   = screenSize
            bBViewHeight.constant      = screenSize
            let sfVideo                = imageThumbnail.size.height/imageThumbnail.size.width
            imageViewWidth.constant    = screenSize/sfVideo
            bBViewWidth.constant       = screenSize/sfVideo
        }
        
        spinnerView.frame               = CGRectMake((imageView.frame.size.width - 100)/2, (imageView.frame.size.height - 100)/2, 100, 100)
        spinnerView.layer.cornerRadius  = 10.0
        spinnerView.hidden              = true
        
        let origin = CGPointMake(0 , UIScreen.mainScreen().bounds.size.height - (CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height + 120))
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait,origin: origin)
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3589571889245561/6463841756"
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kDFPSimulatorID,"a976aeed5251d82899db95f6bc4ab1a7","55a52af80cba60b60e3045dfa9bfcd9d"]
        bannerView.loadRequest(request)
        view.addSubview(bannerView)
    }
    
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("add appeared")
        bannerView.hidden = false
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        bannerView.hidden = true
        print("add failed to appear", error.localizedDescription)
    }
    
    func imageTapped() {
        player               = AVPlayer(URL: playVid!)
        playerLayer          = AVPlayerLayer(player: player)
        playerLayer.frame    = self.imageView.bounds
        playimageView.hidden = true
        self.imageView.layer.addSublayer(playerLayer)
        player.play()
        
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(AudioViewController.playerItemDidReachEnd(_:)),name: AVPlayerItemDidPlayToEndTimeNotification,object: player.currentItem)
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            playerItem.seekToTime(kCMTimeZero)
        }
    }
    
    @IBAction func saveVideo(sender: AnyObject) {
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setURL(playVid, forKey: "VideoNSURL")
        navigationController?.popViewControllerAnimated(true)
    }
}

extension AudioViewController : MPMediaPickerControllerDelegate {
    
    func mediaPicker(mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        self.dismissViewControllerAnimated(true) { () -> Void in
        }
        selectedSong            = mediaItemCollection
        let item                = mediaItemCollection.items.first!
        songUrl                 = item.valueForProperty(MPMediaItemPropertyAssetURL) as? NSURL
        let audioAsset          = AVURLAsset(URL: self.songUrl!)
        print("audio Asset",audioAsset)
        let duration            = CMTimeGetSeconds(audioAsset.duration)
        print("audio duration in cmtimeseconds",duration)
        let durationInt:Int     = Int(duration)
        print("audio in duration in int",durationInt)
        trimmer(durationInt)
    }
    
    func mediaPickerDidCancel(mediaPicker: MPMediaPickerController) {
        self.dismissViewControllerAnimated(true,completion:{ () -> Void in
            self.spinnerView.hidden   = true
            self.playimageView.hidden = false
        })
    }
}