import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer
import AVKit
import AssetsLibrary
import GoogleMobileAds
import ARSLineProgress

class ReverseViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, MPMediaPickerControllerDelegate,  UICollectionViewDelegate , UICollectionViewDataSource ,GADBannerViewDelegate {
    
    var imageThumbnail              = UIImage()
    var videoPathURL                = NSURL()
    var playURL                     = NSURL()
    let identifier                  = "CellIdentifier"
    var latestFilePath              = NSString()
    let date                        = NSDate()
    let calendar                    = NSCalendar.currentCalendar()
    var outputUrl                   : NSURL?
    var filePath                    : NSString?
    var path                        : NSString?
    var player                      = AVPlayer()
    var revOriVid                   : NSURL?
    var oriRevVid                   : NSURL?
    var playVid                     : NSURL?
    var playerViewController        = AVPlayerViewController()
    var blackBackgroundNewHeight    = UIScreen.mainScreen().bounds.size.width - 16
    var invisibleButton2            = UIButton()
    var audioURL                    : NSURL?
    var reversedaudioURL            : NSURL?
    var videoWithAudioURL           : NSURL?
    var newHeight                   : CGFloat?
    let screenSize                  = UIScreen.mainScreen().bounds.size.height - 207
    var playerLayer                 = AVPlayerLayer()
    var bannerView                  : GADBannerView!
    var boolean                     : Bool?
    var boolean1                    : Bool?
    var boolean2                    : Bool?
    var reader                      : AVAssetReader!
    var writer                      : AVAssetWriter!
    var mergeURL                    : NSURL?
    
    @IBOutlet weak var imageView             : UIImageView!
    @IBOutlet weak var collectionView        : UICollectionView!
    @IBOutlet weak var playimageView         : UIImageView!
    @IBOutlet weak var previewVideoView      : UIView!
    @IBOutlet weak var blackBackgroundView   : UIView!
    @IBOutlet weak var blackBackgroundHeight : NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight       : NSLayoutConstraint!
    @IBOutlet weak var saveVideo             : UIBarButtonItem!
    @IBOutlet weak var spinnerView           : UIView!
    @IBOutlet weak var imageViewWidth        : NSLayoutConstraint!
    @IBOutlet weak var blackBackgroundWidth  : NSLayoutConstraint!
    @IBOutlet weak var textHolderView        : UIView!
    @IBOutlet weak var textLabelView         : UILabel!
    
    //    MARK: View LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        boolean = false
        boolean1 = false
        playVid          = self.videoPathURL
        imageView.image  = imageThumbnail
        ThemeColor()
        
        // collection view setup
        let mainScreenSize                                  = UIScreen.mainScreen().bounds
        let collectionViewFlowLayout                        = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewFlowLayout.itemSize                   = CGSizeMake(mainScreenSize.width / 4.0, mainScreenSize.height / 5.0)
        collectionViewFlowLayout.minimumInteritemSpacing    = 0.0
        collectionViewFlowLayout.minimumLineSpacing         = 0.0
        collectionViewFlowLayout.sectionInset               = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        collectionView.delegate                             = self
        
        // tap gesture
        let tapGestureRecognizer                = UITapGestureRecognizer(target:self, action:#selector(ReverseViewController.imageTapped))
        playimageView.userInteractionEnabled    = true
        playimageView.addGestureRecognizer(tapGestureRecognizer)
        
        // adding invisible button
        imageView.addSubview(invisibleButton2)
        invisibleButton2.addTarget(self, action: #selector(ReverseViewController.invisibleButton2Tapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    override func viewDidAppear(animated: Bool) {
        ThemeColor()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ReverseViewController.condition), name: "Notify", object: nil);
    }
    
    override func viewWillLayoutSubviews() {
        invisibleButton2.frame = imageView.bounds
    }
    
    override func viewDidDisappear(animated: Bool) {
        playerViewController.view.removeFromSuperview()
        player.pause()
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "Notify", object: nil)
    }
    
    func getNewVideoOrientation() -> UIImageOrientation {
        let asset       = AVAsset(URL: self.videoPathURL)
        let videoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let size        = videoTrack.naturalSize
        let txf         = videoTrack.preferredTransform
        
        if (size.width == txf.tx && size.height == txf.ty) {
            print("***   Video orientation is left")
            return UIImageOrientation.Left
        }else if (txf.tx == 0 && txf.ty == 0) {
            print("***   Video orientation is right")
            return UIImageOrientation.Right
        }else if (txf.tx == 0 && txf.ty == size.width) {
            print("***   Video orientation is dowm")
            return UIImageOrientation.Down
        }else {
            print("***   Video orientation is up")
            return UIImageOrientation.Up
        }
    }
    
    // MARK: REVERSE VIDEO WITH NO AUDIO FUNCTION
    func reverseVideo() {
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let originalAsset           = AVURLAsset(URL: self.videoPathURL)
        let paths : NSArray         = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory      = paths.objectAtIndex(0) as! NSString
        filePath                    = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds).mov")
        latestFilePath              = filePath!
        let reversedAsset : AVAsset = AVUtilities.assetByReversingAsset(originalAsset, outputURL: NSURL.fileURLWithPath(self.filePath as! String))
        exportAsset(reversedAsset)
    }
    
    //  Export Session Started Function
    func exportAsset(asset : AVAsset){
        let exporter                         = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.outputURL                   = NSURL.fileURLWithPath(latestFilePath as String)
        self.filePath                        = self.latestFilePath
        exporter.outputFileType              = AVFileTypeQuickTimeMovie
        exporter.shouldOptimizeForNetworkUse = true
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinish(exporter)
            } )
        }
    }
    
    //  Export Session Finish Function
    func exportDidFinish(session : AVAssetExportSession) -> Void {
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed Exporting the video *")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed")
            print("Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        
        if(session.status == .Failed){
            outputUrl = session.outputURL!
            audioFromVideo()
            textLabelView.text = "Extracting Audio"
        }
    }
    
    //  MARK: AUDIO FROM VIDEO FUNCTION
    func audioFromVideo() {
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let audioAsset          = AVURLAsset(URL: self.videoPathURL)
        let startTime           = kCMTimeZero
        let endTime             = audioAsset.duration
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        let audioPath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)audio.M4A")
        audioURL                = NSURL.fileURLWithPath(audioPath as String)
        let timeRange           = CMTimeRangeMake(startTime, endTime)
        
        let exporter : AVAssetExportSession  = AVAssetExportSession(asset: audioAsset, presetName: AVAssetExportPresetAppleM4A)!
        exporter.outputURL                   = audioURL
        exporter.timeRange                   = timeRange
        exporter.outputFileType              = AVFileTypeAppleM4A
        exporter.shouldOptimizeForNetworkUse = true
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinishAudio(exporter)
            } )
        }
    }
    
    func exportDidFinishAudio(session : AVAssetExportSession) -> NSURL {
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed - Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        if(session.status == .Completed){
            audioURL       = session.outputURL!
            boolean2       = true
            reverseAudio()
            textLabelView.text = "Merging Audio"
        }else {
            print("*** ERROR : Audio File Not Present In The Video **")
            boolean2             = false
            let alert = UIAlertController(title: "Warning!",message: "Audio not present in this video",preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                self.playVid               = self.outputUrl
                self.spinnerView.hidden    = true
                self.textHolderView.hidden = true
                self.playimageView.hidden  = false
                self.playerLayer.hidden    = true
                self.imageView.image       = self.imageThumbnail
            })
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        return audioURL!
    }
    
    func audioFromVideo1() {
        print("3 start")
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let audioAsset          = AVURLAsset(URL: self.videoPathURL)
        let startTime           = kCMTimeZero
        let endTime             = audioAsset.duration
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        let audioPath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)audio.M4A")
        audioURL                = NSURL.fileURLWithPath(audioPath as String)
        let timeRange           = CMTimeRangeMake(startTime, endTime)
        
        let exporter : AVAssetExportSession  = AVAssetExportSession(asset: audioAsset, presetName: AVAssetExportPresetAppleM4A)!
        exporter.outputURL                   = audioURL
        exporter.timeRange                   = timeRange
        exporter.outputFileType              = AVFileTypeAppleM4A
        exporter.shouldOptimizeForNetworkUse = true
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinishAudio1(exporter)
            } )
        }
    }
    
    func exportDidFinishAudio1(session : AVAssetExportSession) -> NSURL {
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed - Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        if(session.status == .Completed){
            audioURL       = session.outputURL!
            boolean2       = true
            self.textLabelView.text  = "Adding Audio To Video"
            videoWithTwiceAudio(mergeURL!)
        }else {
            print("Audio not present while merging")
            self.spinnerView.hidden   = true
            self.playimageView.hidden = false
            self.playerLayer.hidden   = true
            self.imageView.image      = self.imageThumbnail
            self.imageView.reloadInputViews()
            
            self.playVid = self.mergeURL!
        }
        print("3 end")
        return audioURL!
    }
    
    func reverseAudio(){
        let asset = AVURLAsset(URL: audioURL!)
        do{
            reader = try AVAssetReader(asset: asset)
        }catch{
            print("video unable to read")
        }
        let audioTrack          = asset.tracksWithMediaType(AVMediaTypeAudio)[0]
        let audioReadSetting    = [AVFormatIDKey: Int(kAudioFormatLinearPCM)]
        let readerOutput        = AVAssetReaderTrackOutput(track: audioTrack, outputSettings: audioReadSetting)
        reader.addOutput(readerOutput)
        reader.startReading()
        
        let outputSettings      = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVSampleRateKey: 44100.0,AVNumberOfChannelsKey: 2 as NSNumber, AVEncoderBitRateKey: 128000, AVChannelLayoutKey : NSData()]
        let writerInput         = AVAssetWriterInput(mediaType: AVMediaTypeAudio, outputSettings: outputSettings)
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        let components          = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour                = components.hour
        let minutes             = components.minute
        let seconds             = components.second
        let exportPath          = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)reversedAudio.m4a")
        let exportURL           = NSURL.fileURLWithPath(exportPath)
        
        do{
            writer = try AVAssetWriter(URL: exportURL, fileType: AVFileTypeAppleM4A)
        }catch{
            print("video unable to read")
        }
        writerInput.expectsMediaDataInRealTime = false
        writer.shouldOptimizeForNetworkUse = true
        writer.addInput(writerInput)
        writer.startWriting()
        writer.startSessionAtSourceTime(kCMTimeZero)
        
        let samples = NSMutableArray()
        while let sample : CMSampleBufferRef = readerOutput.copyNextSampleBuffer() {
            samples.addObject(sample)
        }
        let reversedSamples : NSArray = samples.reverseObjectEnumerator().allObjects
        
        for  reversedSample in reversedSamples {
            if writerInput.readyForMoreMediaData {
                writerInput.appendSampleBuffer(reversedSample as! CMSampleBufferRef)
            }else{
                NSThread.sleepForTimeInterval(0.05)
            }
        }
        writerInput.markAsFinished()
        let queue : dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
        dispatch_async(queue) {
            self.writer.finishWritingWithCompletionHandler({
                print("finished Writing")
                print("reversed audio url",exportURL)
                self.reversedaudioURL = exportURL
            })
        }
    }
    
    func getOldVideoOrientation(url : NSURL) -> UIImageOrientation {
        let asset       = AVAsset(URL: url)
        let videoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let size        = videoTrack.naturalSize
        let txf         = videoTrack.preferredTransform
        
        if (size.width == txf.tx && size.height == txf.ty) {
            print("***   Video orientation is left")
            return UIImageOrientation.Left
        }else if (txf.tx == 0 && txf.ty == 0) {
            print("***   Video orientation is right")
            return UIImageOrientation.Right
        }else if (txf.tx == 0 && txf.ty == size.width) {
            print("***   Video orientation is dowm")
            return UIImageOrientation.Down
        }else {
            print("***   Video orientation is up")
            return UIImageOrientation.Up
        }
    }
    
    
    // MARK: ORIGINAL AND REVERSE VIDEO FUNCTION
    func reverseVideo1(){
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let originalAsset       = AVURLAsset(URL: self.videoPathURL)
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        self.filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)OR.mov")
        
        self.latestFilePath     = self.filePath!
        let reversedAsset       = AVUtilities.assetByReversingAsset(originalAsset, outputURL: NSURL.fileURLWithPath(self.filePath as! String))
        self.filePath           = self.latestFilePath
        self.exportAsset1(reversedAsset)
    }
    
    //   Export Session Started Function
    func exportAsset1(asset : AVAsset){
        let exporter : AVAssetExportSession  = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.outputURL                   = NSURL.fileURLWithPath(latestFilePath as String)
        self.filePath                        = self.latestFilePath
        exporter.outputFileType              = AVFileTypeQuickTimeMovie
        exporter.shouldOptimizeForNetworkUse = true
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinish1(exporter)
            } )
        }
    }
    
    //   Export Session Finish Function
    func exportDidFinish1(session : AVAssetExportSession) -> Void {
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed - Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        
        if(session.status == .Failed){
            outputUrl   = session.outputURL!
        }
    }
    
    // merge ori and rev function
    func mergeVideoOrigAndRev()-> NSURL {
        print("B Start")
        let components      = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour            = components.hour
        let minutes         = components.minute
        let seconds         = components.second
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        self.filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds).mov")
        self.latestFilePath     = self.filePath!
        
        let composition = AVMutableComposition()
        let trackVideo  = composition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: CMPersistentTrackID())
        var insertTime  = kCMTimeZero
        for index in 1...2 {
            
            let movieUrl = index == 1 ? videoPathURL : outputUrl!
            let sourceAsset = AVURLAsset(URL: movieUrl, options: nil)
            let tracks      = sourceAsset.tracksWithMediaType(AVMediaTypeVideo)
            
            if tracks.count > 0{
                let assetTrack:AVAssetTrack = tracks[0]
                do {
                    try trackVideo.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceAsset.duration), ofTrack: assetTrack, atTime: insertTime)
                } catch _ {
                }
                insertTime = CMTimeAdd(insertTime, sourceAsset.duration)
            }
        }
        
        let mergedOriRevVidUrl   = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)or.mov")
        self.filePath            = mergedOriRevVidUrl
        let orVid                = NSURL.fileURLWithPath(mergedOriRevVidUrl as String)
        oriRevVid                = orVid
        
        let exporter        = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = NSURL.fileURLWithPath(mergedOriRevVidUrl as String)
        if NSFileManager.defaultManager().fileExistsAtPath(exporter!.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter!.outputURL!)
            } catch _ {
            }
        }
        exporter!.outputFileType = AVFileTypeQuickTimeMovie
        exporter!.exportAsynchronouslyWithCompletionHandler({
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                switch exporter!.status{
                case  AVAssetExportSessionStatus.Failed:
                    print("failed \(exporter!.error)")
                case AVAssetExportSessionStatus.Cancelled:
                    print("cancelled \(exporter!.error)")
                default:break
                }
                self.oriRevVid = exporter?.outputURL
                self.mergeURL = self.oriRevVid
                self.audioFromVideo1()
                self.textLabelView.text  = "Extracting Audios"
                print("B End")
            })
        })
        print("reversed original video" , orVid)
        return orVid
    }
    
    // MARK: REVERSE AND ORIGINAL VIDEO FUNCTION
    func reverseVideo2(){
        print("1 start")
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let originalAsset       = AVURLAsset(URL: self.videoPathURL)
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        self.filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds).mov")
        
        self.latestFilePath   = self.filePath!
        let reversedAsset     = AVUtilities.assetByReversingAsset(originalAsset, outputURL: NSURL.fileURLWithPath(self.filePath as! String))
        self.filePath         = self.latestFilePath
        self.exportAsset2(reversedAsset)
    }
    
    //   Export Session Started Function
    
    func exportAsset2(asset : AVAsset){
        let exporter                         = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.outputURL                   = NSURL.fileURLWithPath(latestFilePath as String)
        self.filePath                        = self.latestFilePath
        exporter.outputFileType              = AVFileTypeQuickTimeMovie
        exporter.shouldOptimizeForNetworkUse = true
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinish2(exporter)
            } )
        }
    }
    
    //  Export Session Finish Function
    func exportDidFinish2(session : AVAssetExportSession) -> Void {
        switch (session.status) {
        case .Cancelled :
            print("Cancelled")
            break
        case .Completed:
            print("Completed")
            break
        case .Exporting:
            print("Exporting")
            break
        case .Failed:
            print("Failed - Error :\(session.error)");
            break
        case .Unknown:
            print("Unknown")
            break
        case .Waiting:
            print("Waiting")
            break
        }
        
        if(session.status == .Failed){
            outputUrl   = session.outputURL!
            print("1 end")
            //            playURL     = outputUrl!
        }
    }
    
    // merge rev and ori func
    func mergeVideoRevAndOri(){
        print("2 start")
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        self.filePath          = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds).mov")
        self.latestFilePath    = self.filePath!
        
        let composition = AVMutableComposition()
        let trackVideo  = composition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: CMPersistentTrackID())
        var insertTime  = kCMTimeZero
        for index in 1...2 {
            let movieUrl = index == 1 ? outputUrl! : videoPathURL
            let sourceAsset = AVURLAsset(URL: movieUrl, options: nil)
            let tracks      = sourceAsset.tracksWithMediaType(AVMediaTypeVideo)
            
            if tracks.count > 0{
                let assetTrack:AVAssetTrack = tracks[0]
                do {
                    try trackVideo.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceAsset.duration), ofTrack: assetTrack, atTime: insertTime)
                } catch _ {
                }
                insertTime = CMTimeAdd(insertTime, sourceAsset.duration)
            }
        }
        
        let mergedRevOriVidUrl   = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)n.mov")
        let roVid                = NSURL.fileURLWithPath(mergedRevOriVidUrl as String)
        revOriVid                = roVid
        self.filePath            = mergedRevOriVidUrl
        
        let exporter        = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = NSURL.fileURLWithPath(mergedRevOriVidUrl as String)
        if NSFileManager.defaultManager().fileExistsAtPath(exporter!.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter!.outputURL!)
            } catch _ {
            }
        }
        
        exporter!.outputFileType = AVFileTypeQuickTimeMovie
        exporter!.exportAsynchronouslyWithCompletionHandler({
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                switch exporter!.status{
                case  AVAssetExportSessionStatus.Failed:
                    print("failed \(exporter!.error)")
                case AVAssetExportSessionStatus.Cancelled:
                    print("cancelled \(exporter!.error)")
                default: break
                }
                self.revOriVid = exporter?.outputURL
                self.mergeURL = self.revOriVid
                self.textLabelView.text  = "Extracting Audio"
                print("2 end")
                self.audioFromVideo1()
            })
        })
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: VIDEO WITH TWICE AUDIO FUNCTION
    func videoWithTwiceAudio(videoURL : NSURL){
        let composition  = AVMutableComposition()
        let trackVideo   = composition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: CMPersistentTrackID())
        let trackAudio   = composition.addMutableTrackWithMediaType(AVMediaTypeAudio, preferredTrackID: CMPersistentTrackID())
        var insertTime   = kCMTimeZero
        
        let sourceVideoAsset  = AVURLAsset(URL: videoURL, options: nil)
        let sourceAudioAsset  = AVURLAsset(URL: audioURL!, options: nil)
        let tracks            = sourceVideoAsset.tracksWithMediaType(AVMediaTypeVideo)
        let audios            = sourceAudioAsset.tracksWithMediaType(AVMediaTypeAudio)
        
        if tracks.count > 0{
            
            let assetTrack:AVAssetTrack      = tracks[0]
            do {
                try trackVideo.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceVideoAsset.duration), ofTrack: assetTrack, atTime: insertTime)
            } catch _ {
            }
            
            let assetTrackAudio:AVAssetTrack = audios[0]
            do {
                try trackAudio.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceVideoAsset.duration), ofTrack: assetTrackAudio, atTime: insertTime)
            } catch _ {
            }
            insertTime = CMTimeAdd(insertTime, sourceVideoAsset.duration)
            
            do {
                try trackAudio.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceAudioAsset.duration), ofTrack: assetTrackAudio, atTime: sourceAudioAsset.duration)
            } catch _ {
            }
            insertTime = CMTimeAdd(sourceVideoAsset.duration, sourceVideoAsset.duration)
        }
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let paths : NSArray     = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory  = paths.objectAtIndex(0) as! NSString
        self.filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)TWICEAUDIO.mov")
        
        let exporter        = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL = NSURL.fileURLWithPath(filePath as! String)
        
        if NSFileManager.defaultManager().fileExistsAtPath(exporter!.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter!.outputURL!)
            } catch _ {
            }
        }
        
        videoWithAudioURL           = exporter!.outputURL
        exporter!.outputFileType    = AVFileTypeMPEG4
        exporter!.exportAsynchronouslyWithCompletionHandler({
            switch exporter!.status{
            case  AVAssetExportSessionStatus.Failed:
                print("failed \(exporter!.error)")
            case AVAssetExportSessionStatus.Cancelled:
                print("cancelled \(exporter!.error)")
            default:
                print("complete")
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.checkOrientation(self.videoWithAudioURL!)
            } )
        } )
    }
    
    func condition() {
        print("A start")
        getOldVideoOrientation(playVid!)
        if boolean == true {
            mergeVideoRevAndOri()
            textLabelView.text  = "Merging Videos"
            boolean = false
        } else if boolean1 == true{
            mergeVideoOrigAndRev()
            textLabelView.text  = "Merging Videos"
            boolean1 = false
        } else if boolean2 == true{
            
            let alert = UIAlertController(title: "Audio Options",message: "Want to reverse audio",preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                self.videoWithAudio(self.reversedaudioURL!)
                self.textLabelView.text = "Reversing Audio"
            })
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                self.videoWithAudio(self.audioURL!)
            })
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            
            self.presentViewController(alert, animated: true, completion: nil)
            boolean2 = false
        }
        print("A end")
    }
    
    func checkOrientation(videoURL : NSURL){
        if getNewVideoOrientation() == getOldVideoOrientation(videoURL) {
            playVid = videoURL
            self.spinnerView.hidden   = true
            self.playimageView.hidden = false
            self.playerLayer.hidden   = true
            self.imageView.image      = self.imageThumbnail
            self.imageView.reloadInputViews()
        }else {
            self.textLabelView.text = "Correcting Orientation"
            rotateVideoLeft(videoURL)
        }
    }
    
    
    // MARK: ADDING AUDIO TO THE VIDEO FUNCTION
    func videoWithAudio(url:NSURL){
        let composition       = AVMutableComposition()
        let trackVideo        = composition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: CMPersistentTrackID())
        let trackAudio        = composition.addMutableTrackWithMediaType(AVMediaTypeAudio, preferredTrackID: CMPersistentTrackID())
        var insertTime        = kCMTimeZero
        let sourceVideoAsset  = AVURLAsset(URL: outputUrl!, options: nil)
        let sourceAudioAsset  = AVURLAsset(URL: url, options: nil)
        let tracks            = sourceVideoAsset.tracksWithMediaType(AVMediaTypeVideo)
        var audios            = sourceAudioAsset.tracksWithMediaType(AVMediaTypeAudio)
        
        if tracks.count > 0 {
            let assetTrack:AVAssetTrack     = tracks[0]
            do {
                try trackVideo.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceVideoAsset.duration), ofTrack: assetTrack, atTime: insertTime)
            } catch _ {
            }
            if audios.count > 0 {
                let assetTrackAudio:AVAssetTrack = audios[0]
                do {
                    try trackAudio.insertTimeRange(CMTimeRangeMake(kCMTimeZero,sourceVideoAsset.duration), ofTrack: assetTrackAudio, atTime: insertTime)
                } catch _ {
                }
            } else {
                insertTime = CMTimeAdd(insertTime, sourceVideoAsset.duration)
            }
        }
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        self.filePath          = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds).mov")
        
        let exporter           = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter!.outputURL    = NSURL.fileURLWithPath(filePath as! String)
        
        if NSFileManager.defaultManager().fileExistsAtPath(exporter!.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter!.outputURL!)
            } catch _ {
            }
        }
        
        videoWithAudioURL           = exporter!.outputURL
        exporter!.outputFileType    = AVFileTypeMPEG4
        exporter!.exportAsynchronouslyWithCompletionHandler({
            switch exporter!.status{
            case  AVAssetExportSessionStatus.Failed:
                print("failed \(exporter!.error)")
            case AVAssetExportSessionStatus.Cancelled:
                print("cancelled \(exporter!.error)")
            default:
                print("complete")
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if self.getNewVideoOrientation() == self.getOldVideoOrientation(self.videoWithAudioURL!){
                    self.spinnerView.hidden   = true
                    self.textHolderView.hidden = true
                    self.playimageView.hidden = false
                    self.playerLayer.hidden   = true
                    self.imageView.image      = self.imageThumbnail
                    self.playVid              = self.videoWithAudioURL
                    print("** DONE : Final video with reverse audio URL is ==" ,self.videoWithAudioURL )
                }else {
                    print("Video orientation is not correct will correct it now")
                    self.rotateVideoLeft(self.videoWithAudioURL!)
                    self.textLabelView.text = "Correcting Orientation"
                }
            } )
        } )
    }
    
    func rotateVideoLeft(url : NSURL){
        
        
        let asset           = AVURLAsset(URL: url)
        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        
        let videoSizeMain           = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        let cropSquare              = CGRectMake(0, 0, videoSizeMain.width  , videoSizeMain.height)
        videoComposition.renderSize = CGSizeMake(cropSquare.size.height , cropSquare.size.width)
        
        let instruction       = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration)
        let layerInstruction  = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        var t1 = CGAffineTransformIdentity
        var t2 = CGAffineTransformIdentity
        
        t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height , 0)
        t2 = CGAffineTransformRotate(t1, CGFloat(M_PI_2))
        
        let finalTranform = t2
        
        layerInstruction.setTransform(finalTranform, atTime: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // save rotated video
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)rotatedleft.mov")
        self.latestFilePath    = filePath
        let finalFileURL1 : NSURL!
        finalFileURL1          = NSURL.fileURLWithPath(latestFilePath as String)
        
        let exporter                = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.videoComposition   = videoComposition
        exporter.outputURL          = finalFileURL1
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        if NSFileManager.defaultManager().fileExistsAtPath(exporter.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinishForOrientation(exporter)
                self.playVid              = self.outputUrl
                self.spinnerView.hidden   = true
                self.textHolderView.hidden = true
                self.playimageView.hidden = false
                self.playerLayer.hidden   = true
                self.imageView.image      = self.imageThumbnail
                self.imageView.reloadInputViews()
                
                print("*** DONE: URL of the corrected orientation or reversed video and reversed audio ==",self.outputUrl)
            } )
        }
    }
    
    func exportDidFinishForOrientation(session : AVAssetExportSession) -> Void {
        switch (session.status) {
        case .Cancelled :
            print("1.Cancelled")
            break
        case .Completed:
            print("2.Completed Exporting the video *")
            break
        case .Exporting:
            print("3.Exporting")
            break
        case .Failed:
            print("4.Failed")
            print("4.Error :\(session.error)");
            break
        case .Unknown:
            print("5.Unknown")
            break
        case .Waiting:
            print("6.Waiting")
            break
        }
        
        if(session.status == .Completed){
            outputUrl            = session.outputURL!
            print("** END **")
        }
    }
    
    // collection view starting
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell                 = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath)
        cell.layer.cornerRadius  = 20.0
        cell.layer.masksToBounds = true
        let imageArray           = ["reverseGreyIcon" , "roIcon" , "orIcon"]
        let labelArray           = ["REVERSE","REVERSE+ORIGINAL","ORIGINAL+REVERSE"]
        (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named: "\(imageArray[indexPath.item])")
        (cell.contentView.viewWithTag(20) as! UILabel).text      = labelArray[indexPath.item]
        (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        cell.backgroundColor = UIColor.whiteColor()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell : UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        playerViewController.view.removeFromSuperview()
        player.pause()
        
        if indexPath.item == 0 {
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"reverseSelected")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            
            ARSLineProgress.show()
            textLabelView.text  = "Reversing Video"
            spinnerView.hidden     = false
            textHolderView.hidden  = false
            playimageView.hidden   = true
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.reverseVideo()
            } )
            
        }else if indexPath.item == 1 {
            boolean = true
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"roSelected")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            
            ARSLineProgress.show()
            textLabelView.text  = "Reversing Video"
            spinnerView.hidden     = false
            textHolderView.hidden  = false
            playimageView.hidden   = true
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.reverseVideo2()
            } )
            
        } else if indexPath.item == 2 {
            boolean1 = true
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"orSelected")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            
            ARSLineProgress.show()
            textLabelView.text  = "Reversing Video"
            spinnerView.hidden     = false
            textHolderView.hidden  = false
            playimageView.hidden   = true
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.reverseVideo1()
            } )
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell : UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        if indexPath.item        == 0 {
            cell.backgroundColor                                     = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"reverseGreyIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        } else if indexPath.item == 1 {
            cell.backgroundColor                                     = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"roIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        } else if indexPath.item == 2 {
            cell.backgroundColor                                     = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"roIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        }
    }
    
    // Save Function
    @IBAction func saveVideo(sender: AnyObject) {
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setURL(playVid, forKey: "VideoNSURL")
        navigationController?.popViewControllerAnimated(true)
    }
    
    func ThemeColor() {
        imageView.addSubview(playimageView)
        playimageView.hidden = false
        navigationController!.navigationBar.barTintColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        previewVideoView.backgroundColor                 = UIColor(red: 223/255 , green: 223/255 , blue: 223/255 , alpha: 1.0)
        collectionView.backgroundColor                   = UIColor(red: 255/255 , green: 255/255 , blue: 255/255 , alpha: 1.0)
        
        let oldWidth    = imageThumbnail.size.width
        let scaleFactor = (UIScreen.mainScreen().bounds.size.width - 30)/oldWidth
        newHeight       = imageThumbnail.size.height*scaleFactor
        
        if newHeight < screenSize {
            imageViewHeight.constant                                 = newHeight!
            blackBackgroundHeight.constant                           = newHeight!
            imageViewWidth.constant                                  = blackBackgroundNewHeight
            blackBackgroundWidth.constant                            = blackBackgroundNewHeight
        } else {
            imageViewHeight.constant                                 = screenSize
            blackBackgroundHeight.constant                           = screenSize
            let sfVideo                                              = imageThumbnail.size.height/imageThumbnail.size.width
            imageViewWidth.constant                                  = screenSize/sfVideo
            blackBackgroundWidth.constant                            = screenSize/sfVideo
        }
        
        ARSLineProgress.showOnView(spinnerView)
        textHolderView.layer.cornerRadius   = 10.0
        spinnerView.layer.cornerRadius      = 10.0
        spinnerView.hidden                  = true
        textHolderView.hidden               = true
        invisibleButton2.frame              = imageView.bounds
        
        getNewVideoOrientation()
        
        let origin = CGPointMake(0 , UIScreen.mainScreen().bounds.size.height - (CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height + 120))
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait,origin: origin)
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3589571889245561/6463841756"
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kDFPSimulatorID,"a976aeed5251d82899db95f6bc4ab1a7","55a52af80cba60b60e3045dfa9bfcd9d"]
        bannerView.loadRequest(request)
        view.addSubview(bannerView)
    }
    
    func imageTapped() {
        playerLayer.hidden   = true
        imageView.image      = nil
        player               = AVPlayer(URL: playVid!)
        playerLayer          = AVPlayerLayer(player: player)
        playerLayer.frame    = self.imageView.bounds
        playimageView.hidden = true
        imageView.layer.addSublayer(playerLayer)
        player.play()
        
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(AudioViewController.playerItemDidReachEnd(_:)),name: AVPlayerItemDidPlayToEndTimeNotification,object: player.currentItem)
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            playerItem.seekToTime(kCMTimeZero)
        }
    }
    
    func invisibleButton2Tapped(sender: UIButton!) {
        let playerIsPlaying:Bool = player.rate > 0
        if (playerIsPlaying) {
            player.pause()
        } else {
            player.play()
        }
    }
}