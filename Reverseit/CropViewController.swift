import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer
import AVKit
import GoogleMobileAds
import ARSLineProgress

class CropViewController: UIViewController,UIImagePickerControllerDelegate ,UINavigationControllerDelegate ,UICollectionViewDelegate , UICollectionViewDataSource , MPMediaPickerControllerDelegate , GADBannerViewDelegate {
    
    var imageThumbnail              = UIImage()
    var videoPathURL                = NSURL()
    let date                        = NSDate()
    var latestFilePath              = NSString()
    var finalFileURL                = NSURL()
    let calendar                    = NSCalendar.currentCalendar()
    let identifier                  = "CellIdentifier"
    var player                      = AVPlayer()
    var playerViewController        = AVPlayerViewController()
    let layers                      = CALayer()
    var finalURL                    = NSURL()
    var renderedSquare              = CGFloat()
    var dimension                   : CGRect!
    var image                       : UIImage!
    var newHeight                   : CGFloat!
    var newWidth                    : CGFloat!
    let screenSize                  = UIScreen.mainScreen().bounds.size.height - 207
    let blackBackgroundNewHeight    = UIScreen.mainScreen().bounds.size.width - 16
    var bannerView                  : GADBannerView!
    
    @IBOutlet weak var imageView            : UIImageView!
    @IBOutlet weak var backgroundView       : UIView!
    @IBOutlet weak var blackBackgroundView  : AKImageCropperView!
    @IBOutlet weak var imageViewHeight      : NSLayoutConstraint!
    @IBOutlet weak var saveVideo            : UIBarButtonItem!
    @IBOutlet weak var spinnerView          : UIView!
    @IBOutlet weak var imageViewWidth       : NSLayoutConstraint!
    @IBOutlet weak var bBHeight             : NSLayoutConstraint!
    @IBOutlet weak var bBWidth              : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //View Life Cycle
        ARSLineProgress.showOnView(spinnerView)
        ThemeColor()
        finalURL                        = videoPathURL
        blackBackgroundView.image       = imageThumbnail
        blackBackgroundView.delegate    = self
        spinnerView.layer.cornerRadius  = 10.0
        blackBackgroundView.refresh()
        
        spinnerView.hidden              = true
        spinnerView.layer.cornerRadius  = 10.0
        crop()
    }
    
    override func viewDidDisappear(animated: Bool) {
        playerViewController.view.removeFromSuperview()
        player.pause()
    }
    
    // Crop Frame Function
    
    func crop() {
        ARSLineProgress.showOnView(spinnerView)
        imageView.hidden            = true
        blackBackgroundView.hidden  = false
        blackBackgroundView.refresh()
        
        if blackBackgroundView.overlayViewIsActive {
            blackBackgroundView.showOverlayViewAnimated(true, withCropFrame: nil, completion: { () -> Void in
            })
        }
        else {
            blackBackgroundView.showOverlayViewAnimated(true, withCropFrame: nil, completion: { () -> Void in
            })
        }
    }
    
    // MARK:- Cropping Main Function
    func cropVideo()-> NSURL{
        blackBackgroundView.hidden  = true
        imageView.hidden            = false
        imageView.addSubview(spinnerView)
        spinnerView.hidden          = false
        
        player.pause()
        
        let asset                       = AVURLAsset(URL: videoPathURL)
        let clipVideoTrack              = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        let instruction                 = AVMutableVideoCompositionInstruction()
        instruction.timeRange           = CMTimeRangeMake(kCMTimeZero, asset.duration)
        
        let layerInstruction  = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        let scaleFactorHeight = clipVideoTrack.naturalSize.height/newHeight
        let scaleFactorWidth  = clipVideoTrack.naturalSize.width/newWidth
        let scaleFactorOfX    = clipVideoTrack.naturalSize.width/newWidth
        let scaleFactorOfY    = clipVideoTrack.naturalSize.height/newHeight
        let newX              = dimension.origin.x*scaleFactorOfX
        let newY              = dimension.origin.y*scaleFactorOfY
        
        let cropVideo               = CGRectMake(0, 0, dimension.size.width*scaleFactorWidth, dimension.size.height*scaleFactorHeight)
        videoComposition.renderSize = cropVideo.size
        
        var t1                                  = CGAffineTransformIdentity
        t1                                      = CGAffineTransformMakeTranslation(-newX , -newY)
        let finalTransform                      = t1
        layerInstruction.setTransform(finalTransform, atTime: kCMTimeZero)
        instruction.layerInstructions           = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions           = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // Save Cropped Video
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray                  = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory : NSString    = paths.objectAtIndex(0) as! NSString
        let filePath                         = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)cropped.mov")
        self.latestFilePath                  = filePath
        finalFileURL                         = NSURL.fileURLWithPath(latestFilePath as String)
        
        let exporter : AVAssetExportSession  = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.videoComposition            = videoComposition
        exporter.outputURL                   = finalFileURL
        
        if NSFileManager.defaultManager().fileExistsAtPath(latestFilePath as String) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinish(exporter)
                self.spinnerView.hidden          = true
                self.blackBackgroundView.hidden  = false
                self.navigationController?.popViewControllerAnimated(true)
            } )
        }
        return finalFileURL
    }
    
    func exportDidFinish(session : AVAssetExportSession) -> Void {
        let outputURL   = session.outputURL!
        let asset       = AVURLAsset(URL: outputURL)
        print(asset)
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func doneCropping()->NSURL{
        blackBackgroundView.croppedImage()
        imageThumbnail = blackBackgroundView.croppedImage()
        imageView.image = imageThumbnail
        imageView.frame = blackBackgroundView.frame
        
        if newHeight == nil && newWidth == nil{
            let alert = UIAlertController(title: "Alert",message: "You have not cropped yet!",preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Done", style: .Default, handler: {action in
                self.crop()
                self.imageView.hidden = true
                self.blackBackgroundView.showOverlayViewAnimated(true, withCropFrame: nil, completion: { () -> Void in
                })
            }))
            self.presentViewController(alert, animated:true, completion:nil)
        } else {
            blackBackgroundView.refresh()
            finalURL = self.cropVideo()
        }
        blackBackgroundView.hidden = true
        return finalURL
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell                                                    = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath)
        cell.layer.cornerRadius                                     = 20.0
        cell.layer.masksToBounds                                    = true
        let imageArray                                              = ["cropWhiteIcon"]
        (cell.contentView.viewWithTag(10) as! UIImageView).image    = UIImage(named: "\(imageArray[indexPath.item])")
        let labelArray                                              = ["CROP"]
        (cell.contentView.viewWithTag(20) as! UILabel).text         = labelArray[indexPath.item]
        (cell.contentView.viewWithTag(20) as! UILabel).textColor    = UIColor.whiteColor()
        cell.backgroundColor                                        = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell          = collectionView.cellForItemAtIndexPath(indexPath)!
        if indexPath.item == 0 {
            player.pause()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"cropWhiteIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell         = collectionView.cellForItemAtIndexPath(indexPath)!
        if indexPath.item == 0 {
            imageView.hidden        = true
            cell.backgroundColor    = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"cropGrayIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        }
    }
    
    @IBAction func saveVideo(sender: AnyObject) {
        let newURL       = doneCropping()
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setURL(newURL, forKey: "VideoNSURL")
    }
    
    func ThemeColor() {
        navigationController!.navigationBar.barTintColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        backgroundView.backgroundColor                   = UIColor(red: 223/255 , green: 223/255 , blue: 223/255 , alpha: 1.0)
        
        let oldWidth                = imageThumbnail.size.width
        let scaleFactor             = (UIScreen.mainScreen().bounds.size.width - 30)/oldWidth
        newHeight                   = imageThumbnail.size.height*scaleFactor
        spinnerView.layer.cornerRadius  = 10.0
        
        if newHeight < screenSize {
            imageViewHeight.constant  = newHeight!
            bBHeight.constant         = newHeight!
            imageViewWidth.constant   = blackBackgroundNewHeight
            bBWidth.constant          = blackBackgroundNewHeight
        } else {
            imageViewHeight.constant  = screenSize
            bBHeight.constant         = screenSize
            let sfVideo               = imageThumbnail.size.height/imageThumbnail.size.width
            imageViewWidth.constant   = screenSize/sfVideo
            bBWidth.constant          = screenSize/sfVideo
        }
        let origin = CGPointMake(0 , UIScreen.mainScreen().bounds.size.height - (CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height + 65))
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait,origin: origin)
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3589571889245561/6463841756"
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kDFPSimulatorID,"a976aeed5251d82899db95f6bc4ab1a7","55a52af80cba60b60e3045dfa9bfcd9d"]
        bannerView.loadRequest(request)
        view.addSubview(bannerView)
    }
    
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("add appeared")
        bannerView.hidden = false
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        bannerView.hidden = true
        print("add failed to appear", error.localizedDescription)
    }
}

extension CropViewController : AKImageCropperViewDelegate {
    func cropRectChanged(rect: CGRect)->CGRect {
        newHeight = blackBackgroundView.scrollView.frame.size.height
        newWidth  = blackBackgroundView.scrollView.frame.size.width
        dimension = rect
        return rect
    }
}