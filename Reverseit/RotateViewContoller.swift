import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer
import AVKit
import GoogleMobileAds
import ARSLineProgress

class RotateViewContoller: UIViewController,UIImagePickerControllerDelegate ,UINavigationControllerDelegate ,UICollectionViewDelegate , UICollectionViewDataSource , MPMediaPickerControllerDelegate ,GADBannerViewDelegate {
    
    var imageThumbnail              = UIImage()
    var videoPathURL                = NSURL()
    let date                        = NSDate()
    var latestFilePath              = NSString()
    var finalFileURL                = NSURL()
    var finalFileURL1               = NSURL()
    var finalFileURL2               = NSURL()
    let calendar                    = NSCalendar.currentCalendar()
    let identifier                  = "CellIdentifier"
    var blackBackgroundNewHeight    = UIScreen.mainScreen().bounds.size.width - 16
    var player                      = AVPlayer()
    var playerLayer                 = AVPlayerLayer()
    var playerViewController        = AVPlayerViewController()
    let layers                      = CALayer()
    var fileURL                     = NSURL()
    var newImage                    : UIImage?
    var invisibleButton             = UIButton()
    var newHeight                   : CGFloat?
    let screenSize                  = UIScreen.mainScreen().bounds.size.height - 207
    var url                         : NSURL!
    var bannerView                  : GADBannerView!
    
    @IBOutlet weak var backgroundView       : UIView!
    @IBOutlet weak var blackBackgroundView  : UIView!
    @IBOutlet weak var imageView            : UIImageView!
    @IBOutlet weak var collectionView       : UICollectionView!
    @IBOutlet weak var imageViewHeight      : NSLayoutConstraint!
    @IBOutlet weak var imageViewWidth       : NSLayoutConstraint!
    @IBOutlet weak var bBViewHeight         : NSLayoutConstraint!
    @IBOutlet weak var playimageView        : UIImageView!
    @IBOutlet weak var saveVideo            : UIBarButtonItem!
    @IBOutlet weak var spinnerView          : UIView!
    @IBOutlet weak var bBViewWidth          : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ARSLineProgress.showOnView(spinnerView)
        fileURL          = self.videoPathURL
        imageView.image  = imageThumbnail
        ThemeColor()
        
        // collection view setup
        let screenSize                                      = UIScreen.mainScreen().bounds
        let collectionViewFlowLayout                        = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewFlowLayout.itemSize                   = CGSizeMake(screenSize.width / 5.0, screenSize.height / 5.0)
        collectionViewFlowLayout.minimumInteritemSpacing    = 0.0
        collectionViewFlowLayout.minimumLineSpacing         = 0.0
        collectionViewFlowLayout.sectionInset               = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        
        // tap gesture
        let tapGestureRecognizer                            = UITapGestureRecognizer(target:self, action:#selector(RotateViewContoller.imageTapped))
        playimageView.userInteractionEnabled                = true
        playimageView.addGestureRecognizer(tapGestureRecognizer)
        
        // adding invisible button
        imageView.addSubview(invisibleButton)
        invisibleButton.addTarget(self, action: #selector(RotateViewContoller.invisibleButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    override func viewWillAppear(animated: Bool) {
        ThemeColor()
    }
    
    override func viewDidDisappear(animated: Bool) {
        playerViewController.view.removeFromSuperview()
        player.pause()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        videoOrientation()
    }
    
    // MARK: rotated video 90 degree
    func rotateVideoRight()-> NSURL{
        playerLayer.hidden    = true
        spinnerView.hidden    = false
        playimageView.hidden  = true
        
        let asset           = AVURLAsset(URL: videoPathURL)
        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        
        let videoSizeMain           = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        let cropSquare              = CGRectMake(0, 0, videoSizeMain.width  , videoSizeMain.height)
        videoComposition.renderSize = CGSizeMake(cropSquare.size.height , cropSquare.size.width)
        
        let instruction         = AVMutableVideoCompositionInstruction()
        instruction.timeRange   = CMTimeRangeMake(kCMTimeZero, asset.duration)
        let layerInstruction    = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        var t1 = CGAffineTransformIdentity
        var t2 = CGAffineTransformIdentity
        
        t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height , 0)
        t2 = CGAffineTransformRotate(t1, CGFloat(M_PI_2))
        
        let finalTranform             = t2
        layerInstruction.setTransform(finalTranform, atTime: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // save rotated video
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)rotatedright.mov")
        self.latestFilePath    = filePath
        finalFileURL           = NSURL.fileURLWithPath(latestFilePath as String)
        
        let exporter                = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.videoComposition   = videoComposition
        exporter.outputURL          = finalFileURL
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        
        if NSFileManager.defaultManager().fileExistsAtPath(exporter.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinish(exporter)
                self.spinnerView.hidden   = true
                self.playimageView.hidden = false
                self.videoOrientation()
                self.imageView.reloadInputViews()
                self.videoPathURL = self.finalFileURL
            } )
        }
        return finalFileURL
    }
    
    // MARK: rotated video -90 degree
    func rotateVideoLeft()-> NSURL{
        playerLayer.hidden    = true
        spinnerView.hidden    = false
        playimageView.hidden  = true
        
        let asset           = AVURLAsset(URL: videoPathURL)
        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        
        let videoSizeMain           = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        let cropSquare              = CGRectMake(0, 0, videoSizeMain.width  , videoSizeMain.height)
        videoComposition.renderSize = CGSizeMake(cropSquare.size.height , cropSquare.size.width)
        
        let instruction       = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration)
        let layerInstruction  = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        var t1 = CGAffineTransformIdentity
        var t2 = CGAffineTransformIdentity
        
        t1 = CGAffineTransformMakeTranslation(0 , clipVideoTrack.naturalSize.width)
        t2 = CGAffineTransformRotate(t1, CGFloat(-M_PI_2))
        
        let finalTranform = t2
        
        layerInstruction.setTransform(finalTranform, atTime: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // save rotated video
        
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)rotatedleft.mov")
        self.latestFilePath    = filePath
        finalFileURL1          = NSURL.fileURLWithPath(latestFilePath as String)
        
        let exporter                = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.videoComposition   = videoComposition
        exporter.outputURL          = finalFileURL1
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        if NSFileManager.defaultManager().fileExistsAtPath(exporter.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinish(exporter)
                self.spinnerView.hidden   = true
                self.playimageView.hidden = false
                self.playerLayer.hidden   = true
                self.videoOrientation()
                self.imageView.reloadInputViews()
                self.videoPathURL = self.finalFileURL1
            } )
        }
        return finalFileURL1
    }
    
    // MARK: rotated video 180 degree
    func rotateVideo()-> NSURL{
        playerLayer.hidden      = true
        spinnerView.hidden      = false
        playimageView.hidden    = true
        
        let asset           = AVURLAsset(URL: videoPathURL)
        let clipVideoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        
        let videoComposition            = AVMutableVideoComposition()
        videoComposition.frameDuration  = CMTimeMake(1, 30)
        
        let videoSizeMain           = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        let cropSquare              = CGRectMake(0, 0, videoSizeMain.width  , videoSizeMain.height)
        videoComposition.renderSize = CGSizeMake(cropSquare.size.width , cropSquare.size.height)
        
        let instruction       = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration)
        let layerInstruction  = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        var t1 = CGAffineTransformIdentity
        var t2 = CGAffineTransformIdentity
        
        t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height)
        t2 = CGAffineTransformRotate(t1, CGFloat(M_PI))
        
        let finalTranform = t2
        layerInstruction.setTransform(finalTranform, atTime: kCMTimeZero)
        instruction.layerInstructions = NSArray(object: layerInstruction) as! [AVVideoCompositionLayerInstruction]
        videoComposition.instructions = NSArray(object: instruction) as! [AVVideoCompositionInstructionProtocol]
        
        // save rotated video
        let components = self.calendar.components([ .Hour, .Minute, .Second], fromDate: self.date)
        let hour       = components.hour
        let minutes    = components.minute
        let seconds    = components.second
        
        let paths : NSArray    = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let filePath           = documentsDirectory.stringByAppendingPathComponent("\(hour)\(minutes)\(seconds)rotated.mov")
        self.latestFilePath    = filePath
        finalFileURL2          = NSURL.fileURLWithPath(latestFilePath as String)
        
        let exporter                = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exporter.videoComposition   = videoComposition
        exporter.outputURL          = finalFileURL2
        exporter.outputFileType     = AVFileTypeQuickTimeMovie
        if NSFileManager.defaultManager().fileExistsAtPath(exporter.outputURL!.path!) {
            do {
                try NSFileManager.defaultManager().removeItemAtURL(exporter.outputURL!)
            } catch _ {
            }
        }
        
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.exportDidFinish(exporter)
                self.spinnerView.hidden   = true
                self.playimageView.hidden = false
                self.videoOrientation()
                self.imageView.reloadInputViews()
                self.videoPathURL = self.finalFileURL2
            } )
        }
        return finalFileURL2
    }
    
    func exportDidFinish(session : AVAssetExportSession) -> Void {
        let outputURL = session.outputURL!
        let asset     = AVURLAsset(URL: outputURL)
        print(asset)
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell                 = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath)
        cell.layer.cornerRadius  = 20.0
        cell.layer.masksToBounds = true
        let imageArray          = ["rotateRightIcon" , "rotateLeftIcon" , "flip"]
        let labelArray          = ["RIGHT","LEFT","ROTATE"]
        cell.backgroundColor    = UIColor.whiteColor()
        (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named: "\(imageArray[indexPath.item])")
        (cell.contentView.viewWithTag(20) as! UILabel).text      = labelArray[indexPath.item]
        (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell : UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        if indexPath.item == 0 {
            player.pause()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"rotateRightWhiteIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            ARSLineProgress.show()
            fileURL = self.rotateVideoRight()
            
        } else if indexPath.item == 1 {
            player.pause()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"rotateLeftWhiteIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            fileURL = self.rotateVideoLeft()
            ARSLineProgress.show()
            
        }else if indexPath.item == 2 {
            player.pause()
            cell.backgroundColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"flip_selected")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor.whiteColor()
            fileURL = self.rotateVideo()
            ARSLineProgress.show()
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell : UICollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath)!
        playerViewController.view.removeFromSuperview()
        
        if indexPath.item == 0 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"rotateRightIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
            
        } else if indexPath.item == 1 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"rotateLeftIcon")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
            
        } else if indexPath.item == 2 {
            cell.backgroundColor = UIColor.whiteColor()
            (cell.contentView.viewWithTag(10) as! UIImageView).image = UIImage(named:"flip")
            (cell.contentView.viewWithTag(20) as! UILabel).textColor = UIColor(red: 119/255 , green: 119/255 , blue: 119/255 , alpha: 1.0)
        }
    }
    
    @IBAction func saveVideo(sender: AnyObject) {
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setURL(fileURL, forKey: "VideoNSURL")
        navigationController?.popViewControllerAnimated(true)
    }
    
    func imageTapped() {
        player               = AVPlayer(URL: fileURL)
        getVideoOrientation(fileURL)
        playerLayer          = AVPlayerLayer(player: player)
        playerLayer.frame    = self.imageView.bounds
        playimageView.hidden = true
        imageView.layer.addSublayer(playerLayer)
        player.play()
        
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(AudioViewController.playerItemDidReachEnd(_:)),name: AVPlayerItemDidPlayToEndTimeNotification,object: player.currentItem)
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            playerItem.seekToTime(kCMTimeZero)
        }
    }
    
    func ThemeColor() {
        ARSLineProgress.showOnView(spinnerView)
        navigationController!.navigationBar.barTintColor = UIColor(red: 130/255 , green: 0/255 , blue: 0/255 , alpha: 1.0)
        backgroundView.backgroundColor                   = UIColor(red: 223/255 , green: 223/255 , blue: 223/255 , alpha: 1.0)
        collectionView.backgroundColor                   = UIColor(red: 255/255 , green: 255/255 , blue: 255/255 , alpha: 1.0)
        
        spinnerView.layer.cornerRadius  = 10.0
        spinnerView.hidden              = true
        invisibleButton.frame           = imageView.bounds
        
        let origin = CGPointMake(0 , UIScreen.mainScreen().bounds.size.height - (CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait).height + 120))
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait,origin: origin)
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3589571889245561/6463841756"
        bannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kDFPSimulatorID,"a976aeed5251d82899db95f6bc4ab1a7","55a52af80cba60b60e3045dfa9bfcd9d"]
        bannerView.loadRequest(request)
        view.addSubview(bannerView)
    }
    
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("add appeared")
        
        bannerView.hidden = false
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        bannerView.hidden = true
        print("add failed to appear", error.localizedDescription)
    }
    
    func invisibleButtonTapped(sender: UIButton!) {
        let playerIsPlaying:Bool = player.rate > 0
        if (playerIsPlaying) {
            player.pause();
        } else {
            player.play();
        }
    }
    
    func getVideoOrientation(url : NSURL) -> UIImageOrientation {
        let asset       = AVAsset(URL: url)
        let videoTrack  = asset.tracksWithMediaType(AVMediaTypeVideo).first!
        let size        = videoTrack.naturalSize
        let txf         = videoTrack.preferredTransform
        
        if (size.width == txf.tx && size.height == txf.ty) {
            print("***   Video orientation is left")
            return UIImageOrientation.Left
        }else if (txf.tx == 0 && txf.ty == 0) {
            print("***   Video orientation is right")
            return UIImageOrientation.Right
        }else if (txf.tx == 0 && txf.ty == size.width) {
            print("***   Video orientation is dowm")
            return UIImageOrientation.Down
        }else {
            print("***   Video orientation is up")
            return UIImageOrientation.Up
        }
    }
    
    func videoOrientation() {
        let asset                                       = AVAsset(URL: fileURL)
        let assetImgGenerate                            = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        
        let time        = CMTimeMake(10, 10)
        var img         : CGImageRef!
        do {
            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
        } catch  {
        }
        let frameImg     = UIImage(CGImage: img)
        imageView.image  = frameImg
        newImage         = frameImg
        
        let newImg : UIImage!
        if newImage != nil{
            newImg   = newImage
        }else{
            newImg   = imageThumbnail
        }
        
        imageView.image                 = newImg
        let oldWidth                    = newImg!.size.width
        let scaleFactor                 = (UIScreen.mainScreen().bounds.size.width - 30)/oldWidth
        newHeight                       = newImg!.size.height*scaleFactor
        
        if newHeight < screenSize {
            imageViewHeight.constant    = newHeight!
            bBViewHeight.constant       = newHeight!
            imageViewWidth.constant     = blackBackgroundNewHeight
            bBViewWidth.constant        = blackBackgroundNewHeight
        } else {
            imageViewHeight.constant    = screenSize
            bBViewHeight.constant       = screenSize
            let sfVideo                 = newImg.size.height/newImg.size.width
            imageViewWidth.constant     = screenSize/sfVideo
            bBViewWidth.constant        = screenSize/sfVideo
        }
        imageView.reloadInputViews()
        invisibleButton.frame = imageView.frame
    }
}